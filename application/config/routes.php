<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "Sistema_controller";//'welcome';
$route['404_override'] = '';//Sistema_controller/pagina_no_encontrada
$route['translate_uri_dashes'] = FALSE;

$route["login"] = "Sistema_controller/login";
$route["logout"] = "Sistema_controller/logout";

$route["perfil"] = "Sistema_controller/perfil";
$route["password"] = "Sistema_controller/password";

$route["acerca"] = "Sistema_controller/acerca";
$route["documentacion"] = "Sistema_controller/documentacion";

// ---------- SUPER
// ---------- ADMIN
$route["secadmin(.*)"] = "Secadmin_controller$1";
$route["exportar/(.*)"] = "Admin_exportar_controller/$1";

// ---------- SECUDRA
$route["secudra(.*)"] = "Secudra_controller$1";
/*$route["secudra/inventario"] = "Secudra_controller/inventario";
$route["secudra/inventario_reporte"] = "Secudra_controller/inventario_reporte";
$route["secudra/asignacion"] = "Secudra_controller/asignacion";
$route["secudra/asignacion_reporte_xls"] = "Secudra_controller/asignacion_reporte_xls";
$route["secudra/asignacion_reporte"] = "Secudra_controller/asignacion_reporte";
$route["secudra/sede_recepcion_reporte"] = "Secudra_controller/sede_recepcion_reporte";*/

// ---------- SECSEDE
$route["secsede(.*)"] = "Secsede_controller$1";
/*$route["secsede/recepcion"] = "Secsede_controller/recepcion";
$route["secsede/recepcion_reporte"] = "Secsede_controller/recepcion_reporte";
$route["secsede/retorno_defecto"] = "Secsede_controller/retorno_defecto";
$route["secsede/retorno"] = "Secsede_controller/retorno";
$route["secsede/retorno_reporte"] = "Secsede_controller/retorno_reporte";
$route["secsede/entregado_personal"] = "Secsede_controller/entregado_personal";
$route["secsede/entregado_personal_reporte"] = "Secsede_controller/entregado_personal_reporte";
$route["secsede/entregado_personal_reporte_xls"] = "Secsede_controller/entregado_personal_reporte_xls";*/























