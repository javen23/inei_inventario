<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		
	}
	
	public function obtener_login_data($username, $password){
		$sql = "select
					t1.usuario_id, t1.usuario_username, t1.perfil_id, t2.perfil_nombre, t1.usuario_nombre
				from
					usuario t1 inner join perfil t2 on t1.perfil_id = t2.perfil_id
				where
					t1.usuario_username = '".addslashes($username)."' and t1.usuario_password = '".addslashes($password)."'";
		$query = $this->db->query($sql);
		if($query->num_rows() == 1){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function sedes(){
		$sql = "select * from sede";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function sede_por_id($sede_id){
		$sql = "select * from sede where sede_id = '".addslashes($sede_id)."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function sedes_por_usuario($usuario_id){
		$sql = "select
					t1.usuario_id, t1.sede_id, t2.sede_nombre
				from
					usuario_sede t1 inner join sede t2 on t1.sede_id = t2.sede_id
				where
					usuario_id = '".addslashes($usuario_id)."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function cargos(){
		$sql = "select * from cargo";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function cargo_por_id($cargo_id){
		$sql = "select * from cargo where cargo_id = '".addslashes($cargo_id)."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function inventario_verificar_asignacion_por_codigo($inventario_codigo){
		$sql = "select
					t1.inventario_id, t1.inventario_codigo, t1.inventario_descripcion, t1.asignado_sede_id, t2.sede_nombre, t1.asignado_fecha
				from
					inventario t1 left join sede t2 on t1.asignado_sede_id = t2.sede_id
				where
					t1.inventario_codigo = '".addslashes($inventario_codigo)."'";
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function inventario_asignar_item_a_sede($inventario_codigo, $sede_id, $cargo_id){
		$sql = "update
					inventario
				set
					asignado_sede_id = '".addslashes($sede_id)."', asignado_cargo_id = '".addslashes($cargo_id)."', asignado_fecha = now()
				where
					inventario_codigo = '".addslashes($inventario_codigo)."'
					and (asignado_sede_id is null or asignado_sede_id = '')
					and (asignado_cargo_id is null or asignado_cargo_id = '')
					and (asignado_fecha is null or asignado_fecha = '')
					";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
		
	}
	
	public function inventario_recepcion_por_codigo($inventario_codigo){
		$sql = "select
					t1.inventario_id, t1.inventario_codigo, t1.inventario_descripcion, t1.inventario_observacion, t1.inventario_fecha, t1.recibido_por_usuario_id, t1.recibido_observacion, t1.recibido_fecha, t2.sede_id, t2.sede_nombre
				from
					inventario t1 left join sede t2 on t1.asignado_sede_id = t2.sede_id
				where
					inventario_codigo = '".addslashes($inventario_codigo)."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function inventario_registrar_recepcion($codigo, $sede_id, $usuario_id, $observacion){
		$sql = "update
					inventario
				set
					recibido_por_usuario_id = '".addslashes($usuario_id)."', recibido_observacion = '".addslashes($observacion)."', recibido_fecha = now()
				where
					inventario_codigo = '".addslashes($codigo)."' and asignado_sede_id = '".addslashes($sede_id)."'";
					/*
					and (recibido_por_usuario_id is null or recibido_por_usuario_id = '')
					and (recibido_observacion is null or recibido_observacion = '')
					and (recibido_fecha is null or recibido_fecha = '')
					*/
		/*$sql = "select
					inventario_id, inventario_codigo, m1_entregado_sede_id, m1_recibido_por, m1_recibido_observacion
				from
					inventario
				where
					inventario_codigo = '".addslashes($codigo))."' and m1_entregado_sede_id = '".addslashes($sede_id))."'"; */
		$query = $this->db->query($sql);

		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function inventario_retorno_por_codigo($inventario_codigo){
		$sql = "select
					t1.inventario_id, t1.inventario_codigo, t1.inventario_descripcion, t1.inventario_observacion, t1.inventario_fecha, t1.recibido_por_usuario_id, t1.recibido_fecha, t2.sede_id, t2.sede_nombre, t1.retorno_observacion, t1.retorno_fecha
				from
					inventario t1 left join sede t2 on t1.asignado_sede_id = t2.sede_id
				where
					inventario_codigo = '".addslashes($inventario_codigo)."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function inventario_retorno_registrar($codigo, $sede_id, $usuario_id, $observacion){
		$sql = "update
					inventario
				set
					retorno_por_usuario_id = '".addslashes($usuario_id)."', retorno_observacion= '".addslashes($observacion)."', retorno_defecto = '0', retorno_fecha = now()
				where
					inventario_codigo = '".addslashes($codigo)."' and asignado_sede_id = '".addslashes($sede_id)."'
					and (retorno_por_usuario_id is null or retorno_por_usuario_id = '')
					and (retorno_observacion is null or retorno_observacion = '')
					and (retorno_fecha is null or retorno_fecha = '')";
		$query = $this->db->query($sql);

		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	
	public function inventario_retorno_defecto_registrar($codigo, $sede_id, $usuario_id, $observacion){
		$sql = "update
					inventario
				set
					retorno_por_usuario_id = '".addslashes($usuario_id)."', retorno_observacion= '".addslashes($observacion)."', retorno_defecto = '1', retorno_fecha = now()
				where
					inventario_codigo = '".addslashes($codigo)."' and asignado_sede_id = '".addslashes($sede_id)."'
					and (retorno_por_usuario_id is null or retorno_por_usuario_id = '')
					and (retorno_observacion is null or retorno_observacion = '')
					and (retorno_fecha is null or retorno_fecha = '')";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	
	function personal_por_sede($sede_id){
		$sql = "select
					personal_id, personal_dni, personal_nombre, sede_id
				from
					personal
				where
					sede_id = '".addslashes($sede_id)."'
				order by personal_nombre";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function personal_por_id($personal_id){
		$sql = "select
					personal_id, personal_dni, personal_nombre, sede_id
				from
					personal
				where
					personal_id = '".addslashes($personal_id)."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	function entrega_item_inventario_por_codigo($inventario_codigo){
		$sql = "select
					t1.inventario_id,
					t1.inventario_codigo,
					t1.inventario_descripcion,
					t2.sede_id,
					t2.sede_nombre,
					t3.personal_id,
					t3.personal_dni,
					t3.personal_nombre
				from
					inventario t1
					left join sede t2 on t1.asignado_sede_id = t2.sede_id
					left join personal t3 on t1.distribuido_a_personal_id = t3.personal_id
					 where inventario_codigo = '".addslashes($inventario_codigo)."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function entregado_personal_registrar($sede_id, $personal_id, $inventario_codigo){
		$sql = "update
					inventario
				set
					distribuido_a_personal_id = '".addslashes($personal_id)."',
					distribuido_fecha = now()
				where
					inventario_codigo = '".addslashes($inventario_codigo)."'
					and asignado_sede_id = '".addslashes($sede_id)."'
					and (distribuido_a_personal_id is null or distribuido_a_personal_id = '')
					and (distribuido_fecha is null or distribuido_fecha = '')
		";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
	
	
	// JHV- Inicio
        // Equipos OI x SEDE
        public function sedes_nro_operador_informatico($sede_id)
        {
            $sql = "select sede_oi from sede where sede_id = '".addslashes($sede_id)."'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
        
        public function sedes_guardar_nro_operador_informatico( $sede_id, $oper_informatico)
        {            
            $this->db->where('sede_id', $sede_id);
            $this->db->update('sede', $oper_informatico);
            
            if($this->db->affected_rows() == 1){
            	return true;
            } else {
		return false;
            }
                
        }
        
        public function obtener_total_productos_x_sede  ( $sede_id, $producto)
        {
            $sql = "select count(inventario_descripcion) as total from inventario where asignado_sede_id = '".addslashes($sede_id)."' and inventario_descripcion = '".addslashes($producto)."'";
            $query = $this->db->query($sql);
            return $query->row_array();            
        }
        
        public function obtener_productos_inoperativos_sede  ( $sede_id, $producto)
        {
            $sql = "select count(estado) as inoperativo from inventario where asignado_sede_id = '".addslashes($sede_id)."' and inventario_descripcion = '".addslashes($producto)."' and estado=1";
            $query = $this->db->query($sql);
            return $query->row_array();            
        }        
        // JHV- Fin 
	
}

?>