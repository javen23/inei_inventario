<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model("System_model", "model");
	}
	
	public function index(){
		$vars = array(
			"title" => "Este es el título",
			"description" => "hola esta es una descripción para el tag meta",
			"css" => array(
				base_url("assets/theme/css/1.css"),
				base_url("assets/theme/css/1.css"),
				"http://....."
			),
			"js" => array(
				base_url("assets/theme/js/1.js"),
				"http://..../1.js"
			),
			"message" => array(
				"type" => "success",
				"text" => "Este es un mensaje todo lo que se tenga aquí se formateará en html !\"#$%&/()="
			),
			"body" => "este es el contenido en </strong>html</strong> que se agregará al body, quiza no sea necsario",
			"view" => "test/test_view",
			"data" => array(
				"var_1" => "informacion que será envada",
				"var_2" => array(1, 2, 3, 4, 5),
				"var_3" => "otros datos"
			)
		);
		$this->load->view("main_view", $vars);
	}
	
	public function test(){
		$this->load->view("test/test_view", $vars);
	}
	
	public function test_reporte(){
		
		$this->load->view("test/test_reporte_view", $vars);
	}
	
}
	
?>