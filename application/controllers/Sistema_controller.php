<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_controller extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model("Sistema_model", "model");
	}
	
	public function pagina_no_encontrada(){
		$vars = array(
			"title" => "Página no encontrada",
			"description" => "Página no encontrada",
			"view" => "sistema/pagina_no_encontrada_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function acerca(){
		$vars = array(
			"title" => "Acerca",
			"view" => "sistema/acerca_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function documentacion(){
		$vars = array(
			"title" => "Documentación",
			"view" => "sistema/documentacion_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function perfil(){
		$vars = array(
			"title" => "Mi perfil",
			"view" => "sistema/perfil_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function password(){
		$vars = array(
			"title" => "Olvidé mi contraseña",
			"view" => "sistema/password_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function index(){
		$vars = array(
			"description" => "Sistema de Inventario Electrónico del Proyecto EDNOM 2016 - INEI",
			"view" => "sistema/principal_view"
		);
		$this->load->view("template_view", $vars);
	}
	
	public function login(){
		$vars = array(
			"description" => "Sistema de Inventario Electrónico del Proyecto EDNOM 2016 - INEI",
			"view" => "sistema/login_view"
		);
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "login"){
				$data = $this->model->obtener_login_data($this->input->post("username"), $this->input->post("password"));
				if($data){
					$this->session->set_userdata(array("login_data" => $data));
					redirect("");
				}else{
					$vars["mensaje"] = array(
						"type" => "danger",
						"text" => "Login incorrecto, inténtelo nuevamente."
					);
				}
			}
		}
		
		$this->load->view("template_view", $vars);
	}
	
	public function logout(){

		$session = $this->session->userdata();
		foreach($session as $key => $value){
			$this->session->unset_userdata($key);
		}
		redirect("login");
	}
}
?>
