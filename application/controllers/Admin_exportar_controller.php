<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_exportar_controller extends CI_Controller 
{

	private $alignment_general;
	private $style_head;	
	private $style_sede;
        private $style_contenido;
        private $style_sumatoria;
        
	function __construct()
	{
		parent::__construct();

		$this->load->library('PHPExcel');
		$this->load->model("Sistema_model", "model");
		ini_set("memory_limit", "1024M");
	}
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	function set_styles()
	{
		$this->alignment_general = array(
			'alignment' => array(
				//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
			),
		);

		$this->style_head = array(
			'alignment' => array(
                                'wrap' => true,
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'font' => array(
				'name' => 'Calibri',
				'size' => 8,
                                'bold' => true
			),
			'borders' => array(
				'top' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'right' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'left' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'bottom' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$this->style_sede = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			),
			'font' => array(
                                'name' => 'Calibri',
				'size' => 8,
			),
			'borders' => array(
				'top' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'right' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'left' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'bottom' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$this->style_contenido = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),                    
			'font' => array(
                                'name' => 'Calibri',
				'size' => 8
			),
			'borders' => array(
				'top' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'right' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'left' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'bottom' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)                    
		);
		$this->style_sumatoria = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),                    
			'font' => array(
                                'name' => 'Calibri',
				'size' => 8,
                                'bold' => true                            
			),
			'borders' => array(
				'top' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'right' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'left' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
				'bottom' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)                    
		);                
	}

	function cell_value_with_merge($cell, $content, $merge)
	{
		$this->sheet->setCellValue($cell,$content);
		$this->sheet->mergeCells($merge);
	}
        
	function cell_value_not_merge($cell, $content)
	{
		$this->sheet->setCellValue($cell,$content);
	}

	public function excel_equipos_oi_sedes()
	{
            ////////////////////////////////
            //Colores y Estilos
            ////////////////////////////////
            $this->set_styles();

            ////////////////////////////////
            // Sheet 1 : EQUIPOS
            ////////////////////////////////
            $nro_sheet = 0;
            
            // Sedes
            $sedes = $this->model->sedes();
            // Generar reporte       
            $arraySedes = array();
            $itemSede = array();
            
            foreach($sedes as $row) {
                //Sede
                $itemSede['sede_nombre'] = $row['sede_nombre'];   
                //Nro OI
                $itemSede['sede_oi'] = $row['sede_oi']; 
                // Tablets
                $tableta = "TABLET YOGA 8.2";
                $total_tabletas_x_sede = $this->model->obtener_total_productos_x_sede($row["sede_id"], $tableta);
                $itemSede['tablets'] = $total_tabletas_x_sede['total'];
                // Lectoras
                $lectora = "LECTORA DE CODIGO DE BARRA BARCODE CT10X";
                $total_lectoras_x_sede = $this->model->obtener_total_productos_x_sede($row["sede_id"], $lectora); 
                $itemSede['lectoras'] = $total_lectoras_x_sede['total'];
                // Tablet y Lectora
                if ($total_tabletas_x_sede['total'] < $total_lectoras_x_sede['total']) {
                    $menor = $total_tabletas_x_sede['total'];
                } else {
                    $menor = $total_lectoras_x_sede['total'];
                }
                $itemSede['tablet_lectora'] = $menor;
                // Tablet Inoperativa
                $total_tabletas_inoperativos = $this->model->obtener_productos_inoperativos_sede($row["sede_id"], $tableta);
                $itemSede['tablets_inoperativos'] = $total_tabletas_inoperativos['inoperativo'];                
                // Lectora Inoperativa
                $total_lectoras_inoperativos = $this->model->obtener_productos_inoperativos_sede($row["sede_id"], $lectora);
                $itemSede['letoras_inoperativos'] = $total_lectoras_inoperativos['inoperativo'];  
                // Tablet Operativo
                $tabletOperativo = $itemSede['tablets'] - $itemSede['tablets_inoperativos'];
                // Lectora Operativo
                $lectoraOperativo = $itemSede['lectoras'] - $itemSede['letoras_inoperativos'];
                // Tablet Lectora Operativo
                if($tabletOperativo < $lectoraOperativo) {
                    $tabletLectoraOperativo = $tabletOperativo;
                } else {
                    $tabletLectoraOperativo = $lectoraOperativo;
                }                 
                $itemSede['tablet_lectora_operativo'] = $tabletLectoraOperativo;
                                
                // Excedente Tablets
                $itemSede['excedente_tablets'] = $itemSede['tablets'] -  $itemSede['sede_oi'] - $itemSede['tablets_inoperativos'];
                // Excedente Lectoras
                $itemSede['excedente_lectoras'] = $itemSede['lectoras'] -  $itemSede['sede_oi'] - $itemSede['letoras_inoperativos'];
                // Disponible Tablet Lectora
                if($itemSede['excedente_tablets'] < $itemSede['excedente_lectoras']) {
                    $disponibleTabletLectora = $itemSede['excedente_tablets'];
                } else {
                    $disponibleTabletLectora = $itemSede['excedente_lectoras'];
                }                
                $itemSede['diponibles_tablet_lectora'] = $disponibleTabletLectora;
                // Disponible Tablets
                $itemSede['diponibles_tablets'] = $itemSede['excedente_tablets'] - $itemSede['diponibles_tablet_lectora'];                
                // Disponible Lecotras
                $itemSede['diponibles_lectoras'] = $itemSede['excedente_lectoras'] - $itemSede['diponibles_tablet_lectora'];
                                
                array_push($arraySedes,$itemSede);
            }
            
            $name_sheet = 'EQUIPOS';
            $back_color = '366092';
            
            $valores = array( 'nro_sheet' => $nro_sheet, 'dataSedes' => $arraySedes, 'back_color' => $back_color, 'name_sheet' => $name_sheet );
            
            $this->sheet_base( $valores );

            ////////////////////////////////
            $this->phpexcel->getProperties()
            ->setTitle("INEI - ")
            ->setDescription("Equipos");

            header("Content-Type: application/vnd.ms-excel");
            $nombreArchivo =  'EQUIPOS-OI-SEDES_'.date('Y-m-d');
            header("Content-Disposition: attachment; filename=\"$nombreArchivo.xls\""); 
            header("Cache-Control: max-age=0");
            // Genera Excel
            $writer = PHPExcel_IOFactory::createWriter($this->phpexcel, "Excel5");
            $writer->save('php://output');
            exit;
	}

	public function sheet_base( $variable_array )
	{
		if ( $variable_array['nro_sheet'] == 0 ) {
			// pestaña
			$this->sheet = $this->phpexcel->getActiveSheet(0);
		} else {
			$this->sheet = $this->phpexcel->createSheet( $variable_array['nro_sheet'] );
		}
		
		////////////////////////////////
		// Formato de la hoja ( Set Orientation, size and scaling )
		////////////////////////////////
		$this->sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);// horizontal ORIENTATION_LANDSCAPE
		$this->sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->sheet->getDefaultStyle()->getFont()->setName('Calibri');
		$this->sheet->getDefaultStyle()->applyFromArray($this->alignment_general);
		$this->sheet->getSheetView()->setZoomScale(95);
		$this->sheet->getDefaultColumnDimension()->setWidth(8); //default size column
		$this->sheet->getDefaultRowDimension()->setRowHeight(12.6);

		//$this->sheet->getColumnDimension('F')->setWidth(5);

		// custom page margins
		$pageMargins = $this->sheet->getPageMargins();
		$pageMargins->setTop(0.2);
		$pageMargins->setBottom(0);
		$pageMargins->setLeft(0.2);
		$pageMargins->setRight(0);
		// .custom page margins

		////////////////////////////////
		// Cuerpo
		////////////////////////////////
		$indice = 2; //fila inicial
                $pagina = 0;
                        
		//$sql = $variable_array['sql'];
		//$query = $this->convert_utf8->convert_result( $this->fotocheck_model->only_query( $sql ) );

		foreach ($variable_array['dataSedes'] as $sedes) 
		{ 
			$title_line1 = 1;                        
			$this->cell_value_not_merge( 'A'.$title_line1, 'SEDE');
                        $this->cell_value_not_merge( 'B'.$title_line1, 'NRO. OI');
			$this->cell_value_not_merge( 'C'.$title_line1, 'TABLETS');
                        $this->cell_value_not_merge( 'D'.$title_line1, 'LECTORAS');
			$this->cell_value_not_merge( 'E'.$title_line1, 'TABLET Y LECTORA');
                        $this->sheet->getCell('F'.$title_line1)->setValue("TABLET\nINOPERATIVA");                        
                        $this->sheet->getCell('G'.$title_line1)->setValue("LECTORA\nINOPERATIVA");
                        $this->sheet->getCell('H'.$title_line1)->setValue("TABLET Y LECTORA\nOPERATIVO");                        
                        $this->sheet->getCell('I'.$title_line1)->setValue("TABLET\nEXCEDENTE");                        
                        $this->sheet->getCell('J'.$title_line1)->setValue("LECTORA\nEXCEDENTE");                        
                        $this->sheet->getCell('K'.$title_line1)->setValue("TABLET Y LECTORA\nEXCEDENTE");                        
                        $this->sheet->getCell('L'.$title_line1)->setValue("TABLET\nRESTANTE");                        
                        $this->sheet->getCell('M'.$title_line1)->setValue("LECTORA\nRESTANTE");                                                
                        $this->sheet->getStyle( 'A'.$title_line1.':'.'M'.$title_line1 )->applyFromArray( $this->style_head );
                        $this->sheet->getRowDimension('1')->setRowHeight(24);
                        
                        $this->cell_value_not_merge( 'A'.$indice, $sedes['sede_nombre'] );
                        $this->sheet->getStyle('A'.$indice)->applyFromArray( $this->style_sede );
                        $this->sheet->getColumnDimension('A')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'B'.$indice, $sedes['sede_oi'] );
                        $this->sheet->getStyle('B'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('B')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'C'.$indice, $sedes['tablets'] );
                        $this->sheet->getStyle('C'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('C')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'D'.$indice, $sedes['lectoras'] );
                        $this->sheet->getStyle('D'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('D')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'E'.$indice, $sedes['tablet_lectora'] );
                        $this->sheet->getStyle('E'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('E')->setAutoSize(true);
                        if($sedes['tablet_lectora'] > 1) {
                            $this->sheet->getStyle('E'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8FBC8F');
                        } else {
                            $this->sheet->getStyle('E'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF4040');
                        }                        
                        
                        $this->cell_value_not_merge( 'F'.$indice, $sedes['tablets_inoperativos'] );
                        $this->sheet->getStyle('F'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('F')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'G'.$indice, $sedes['letoras_inoperativos'] );
                        $this->sheet->getStyle('G'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('G')->setAutoSize(true);
                            
                        $this->cell_value_not_merge( 'H'.$indice, $sedes['tablet_lectora_operativo'] );
                        $this->sheet->getStyle('H'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('H')->setAutoSize(true);
                        if($sedes['tablet_lectora'] > 1) {
                            $this->sheet->getStyle('H'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8FBC8F');
                        } else {
                            $this->sheet->getStyle('H'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF4040');
                        }                        
                        
                        $this->sheet->setCellValue( 'I'.$indice, '=(C'.$indice.' - B'.$indice.' - F'.$indice.')');
                        $this->sheet->getStyle('I'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('I')->setAutoSize(true);

                        $this->cell_value_not_merge( 'J'.$indice, '=(D'.$indice.' - B'.$indice.' - G'.$indice.')');
                        $this->sheet->getStyle('J'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('J')->setAutoSize(true);
                        
                        $this->cell_value_not_merge( 'K'.$indice, $sedes['diponibles_tablet_lectora'] );
                        $this->sheet->getStyle('K'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('K')->setAutoSize(true);
                        if($sedes['diponibles_tablet_lectora'] > 1) {
                            $this->sheet->getStyle('K'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8FBC8F');
                        } else {
                            $this->sheet->getStyle('K'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF4040');
                        }
                        
                        $this->cell_value_not_merge( 'L'.$indice, '=(I'.$indice.' - K'.$indice.')');
                        $this->sheet->getStyle('L'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('L')->setAutoSize(true);
                        if($sedes['diponibles_tablets'] > 1) {
                            $this->sheet->getStyle('L'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8FBC8F');
                        } else {
                            $this->sheet->getStyle('L'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF4040');
                        }
                        
                        $this->cell_value_not_merge( 'M'.$indice,  '=(J'.$indice.' - K'.$indice.')');
                        $this->sheet->getStyle('M'.$indice)->applyFromArray( $this->style_contenido );
                        $this->sheet->getColumnDimension('M')->setAutoSize(true);
                        if($sedes['diponibles_lectoras'] > 1) {
                            $this->sheet->getStyle('M'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8FBC8F');
                        } else {
                            $this->sheet->getStyle('M'.$indice)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF4040');
                        }
                        
                        /*
                        // Salto de pagina cada multiplo de 4                        
                        if (fmod($contador,4) == 0)
                        {
                            $pagina++;                            
                            // Imprime en $pagina respecto a la fila $salto
                            $salto = 54*$pagina;
                            $this->sheet->setBreak("A".$salto, PHPExcel_Worksheet::BREAK_ROW );
                            
                            // Para que pinte en la posicion $indice
                            $indice = 54*$pagina + 1;
                            
                        }
                        */
                        $indice ++;
          
                }   
                //Sumatorias
                $indice_anterior = $indice - 1;
                $this->cell_value_not_merge( 'A'.$indice, 'TOTAL');
                $this->sheet->getStyle('A'.$indice)->applyFromArray( $this->style_sumatoria );
                
                $this->sheet->setCellValue( 'B'.$indice, '=SUM(B2:B'.$indice_anterior.')');
                $this->sheet->getStyle('B'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'C'.$indice, '=SUM(C2:C'.$indice_anterior.')');
                $this->sheet->getStyle('C'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'D'.$indice, '=SUM(D2:D'.$indice_anterior.')');
                $this->sheet->getStyle('D'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'E'.$indice, '=SUM(E2:E'.$indice_anterior.')');
                $this->sheet->getStyle('E'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'F'.$indice, '=SUM(F2:F'.$indice_anterior.')');
                $this->sheet->getStyle('F'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'G'.$indice, '=SUM(G2:G'.$indice_anterior.')');
                $this->sheet->getStyle('G'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'H'.$indice, '=SUM(H2:H'.$indice_anterior.')');
                $this->sheet->getStyle('H'.$indice)->applyFromArray( $this->style_sumatoria );
                
                $this->sheet->setCellValue( 'I'.$indice, '=SUM(I2:I'.$indice_anterior.')');
                $this->sheet->getStyle('I'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'J'.$indice, '=SUM(J2:J'.$indice_anterior.')');
                $this->sheet->getStyle('J'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'K'.$indice, '=SUM(K2:K'.$indice_anterior.')');
                $this->sheet->getStyle('K'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'L'.$indice, '=SUM(L2:L'.$indice_anterior.')');
                $this->sheet->getStyle('L'.$indice)->applyFromArray( $this->style_sumatoria );

                $this->sheet->setCellValue( 'M'.$indice, '=SUM(M2:M'.$indice_anterior.')');
                $this->sheet->getStyle('M'.$indice)->applyFromArray( $this->style_sumatoria );
                
                ////////////////////////////////
		// SALIDA EXCEL ( Propiedades del archivo excel )
		////////////////////////////////
		$this->sheet->setTitle( $variable_array['name_sheet'] );
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */