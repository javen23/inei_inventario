<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Secudra_controller extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model("Sistema_model", "model");
		$this->load->library('grocery_CRUD');
	}
	
	public function index(){
		
	}
	
	public function inventario(){
		$vars = array(
			"title" => "Registrar ítems en inventario",
			"view" => "template_crud"
		);
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();//util para solo reportes
		//$crud->set_theme("bootstrap");
		//$crud->set_language("spanish");
		$crud->set_table("inventario");
		$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha");
		$crud->fields("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha");
		$crud->required_fields("inventario_codigo", "inventario_descripcion");
		//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
		$crud->unset_texteditor("inventario_observacion", "full_text");
		$crud->callback_before_insert(array($this, "inventario____before_insert"));
		$crud->display_as(array(
			"inventario_codigo" => "CODIGO",
			"inventario_descripcion" => "DESCRIPCION",
			"inventario_observacion" => "OBSERVACION",
			"inventario_fecha" => "FECHA"
		));
		$crud->field_type("inventario_descripcion", "dropdown", array(
			"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
			"Lector de código de barras TC10" => "Lector de código de barras TC10"
		));
		$crud->field_type("inventario_fecha", "invisible");
		$crud->set_subject("inventario");//nombre que se mostrara, ejm: "Agregar usuario"
		$crud->unset_delete();
		//$crud->unset_operations();
		

		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		$this->load->view("template_view", $vars);
	}
	function inventario____before_insert($post_array, $primary_key){
		$post_array["inventario_fecha"] = date("Y-m-d H:i:s");
		return $post_array;
	}
	
	public function inventario_reporte(){
		$vars = array(
			"title" => "Ítems en inventario",
			"view" => "template_crud"
		);
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();//util para solo reportes
		//$crud->set_theme("bootstrap");
		//$crud->set_language("spanish");
		$crud->set_table("inventario");
		$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha");
		$crud->fields("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha");

		
		
		//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
		$crud->unset_texteditor("inventario_observacion", "full_text");
		//$crud->callback_before_insert(array($this, "inventario____before_insert"));
		$crud->display_as(array(
			"inventario_codigo" => "CODIGO",
			"inventario_descripcion" => "DESCRIPCION",
			"inventario_observacion" => "OBSERVACION",
			"inventario_fecha" => "FECHA"
		));
		$crud->field_type("inventario_descripcion", "dropdown", array(
			"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
			"Lector de código de barras TC10" => "Lector de código de barras TC10"
		));
		$crud->field_type("inventario_fecha", "invisible");
		$crud->set_subject("inventario");//nombre que se mostrara, ejm: "Agregar usuario"
		
		$crud->unset_read();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		//$crud->unset_operations();
		
		// ---------- CRUD OUTPUT
		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		$this->load->view("template_view", $vars);
	}
	
	public function asignacion(){

		$session_var_name = "asignacion_data";
		
		$vars = array(
			"title" => "Registrar asociación sede-ítem",
			"view" => "secudra/asignacion_view"
		);
		

		$session_var_data = array(
			"sede" => array(
				/*
				"sede_id" => "",
				"sede_nombre" => ""
				*/
			),
			"cargo" => array(
				/*
				"cargo_id" => "",
				"cargo_nombre" => ""
				*/
			),
			"inventario" => array(
				/*
				array(
					"inventario_id" => "",
					"inventario_codigo" => "",
					"inventario_descripcion" => "",
					"sede_id" => "",
					"sede_nombre" => "",
					"cargo_id" => "",
					"cargo_nombre" => ""
				)
				*/
			)
		);
		$vars["data"]["sedes"] = $this->model->sedes();
		$vars["data"]["cargos"] = $this->model->cargos();

		if($this->session->userdata($session_var_name)){
			$session_var_data = $this->session->userdata($session_var_name);
		}
	
		

		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$session_var_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
					
					//$session_var_data["cargo"] = array();
					//$session_var_data["inventario"] = array();
				}else{
					/*$vars["message2"] = array(
						"type" => "danger",
						"text" => "No se ha podido seleccionar la sede."
					);*/
					$session_var_data["sede"] = array();
					/*$session_var_data["cargo"] = array();
					$session_var_data["inventario"] = array();*/
				}
			}else if($this->input->post("accion") == "seleccionar-cargo"){
				$cargo_id = $this->input->post("cargo_id");
				
				$result = $this->model->cargo_por_id($cargo_id);
				
				if($result){
					$session_var_data["cargo"] = array(
						"cargo_id" => $result["cargo_id"],
						"cargo_nombre" => $result["cargo_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado el cargo \"".$result["cargo_nombre"]."\"."
					);
				}else{
					/*$vars["message2"] = array(
						"type" => "danger",
						"text" => "No se ha podido seleccionar el cargo."
					);*/
					$session_var_data["cargo"] = array();
				}
			}else if($this->input->post("accion") == "agregar-codigo"){
				$codigo = trim($this->input->post("codigo"));
				

				if(!array_key_exists($codigo, $session_var_data["inventario"])){

					$result = $this->model->inventario_verificar_asignacion_por_codigo($codigo);
					if(count($result) == 1){
						if($result[0]["asignado_sede_id"] == ""){

							$session_var_data["inventario"][$codigo] = array(
								"inventario_id" => $result[0]["inventario_id"],
								"inventario_codigo" => $result[0]["inventario_codigo"],
								"inventario_descripcion" => $result[0]["inventario_descripcion"],
								"sede_id" => $session_var_data["sede"]["sede_id"],
								"sede_nombre" => $session_var_data["sede"]["sede_nombre"],
								"cargo_id" => $session_var_data["cargo"]["cargo_id"],
								"cargo_nombre" => $session_var_data["cargo"]["cargo_nombre"]
							);

							$vars["message2"] = array(
								"type" => "success",
								"text" => "El código \"".$codigo."\" se agregó satisfactoriamente."
							);
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "El código \"".$codigo."\" (".$result[0]["inventario_descripcion"].") se ha asignado a la sede \"".$result[0]["sede_nombre"]."\" en la fecha ".$result[0]["asignado_fecha"]."."
							);
						}
					}else{
						$vars["message2"] = array(
							"type" => "danger",
							"text" => "El código \"".$codigo."\" no se encuentra en la base de datos."
						);
					}
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "El código \"".$codigo."\" ya se encuentra en el listado temporal."
					);
				}
			}else if($this->input->post("accion") == "eliminar-codigo"){
				$codigo = $this->input->post("codigo");
				if(array_key_exists($codigo, $session_var_data["inventario"])){
					unset($session_var_data["inventario"][$codigo]);

					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha eliminado el código \"".$codigo."\" satisfactoriamente."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "El código \"".$codigo."\" no se encuentra en el listado temporal."
					);
				}
			}else if($this->input->post("accion") == "asignar-y-terminar"){
				if(count($session_var_data["inventario"]) > 0){
					$bien = array();
					$mal = array();
					foreach($session_var_data["inventario"] as $key => $value){
						if($this->model->inventario_asignar_item_a_sede($value["inventario_codigo"], $value["sede_id"], $value["cargo_id"])){
							$bien[] = $value["inventario_codigo"];
						}else{
							$mal[] = $value["inventario_codigo"];
						}
					}
					
					if(count($bien) > 0){
						if(count($mal) == 0){
							$vars["message2"] = array(
								"type" => "success",
								"text" => "Los siguientes items se han asignado correctamente: [".implode(",", $bien)."]."
							);
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "Los siguientes items se han asignado correctamente: [".implode(",", $bien)."], sin embargo los siguientes items presentaron un error: [".implode(",", $mal)."]."
							);
						}
						$session_var_data["inventario"] = array();
					}else{
						$vars["message2"] = array(
							"type" => "danger",
							"text" => "No se ha podido asignar ninguno de los items."
						);
					}

					/*
					$inventario_codigos = array();
					foreach($session_var_data["inventario"] as $key => $value){
						$inventario_codigos[] = $value["inventario_codigo"];
					}
					
					if($this->model->inventario_asignar_a_sede($session_var_data["sede"]["sede_id"], $inventario_codigos)){
						$vars["message2"] = array(
							"type" => "success",
							"text" => "Se asignaron los siguientes codigos patrimoniales [".implode(",", $inventario_codigos)."] a la sede \"".$session_var_data["sede"]["sede_nombre"]."\"."
						);
						
						$session_var_data = array("sede" => array(), "inventario" => array());// limpiar el array
					}else{
						$output["message2"] = array(
							"type" => "success",
							"text" => "Hubo un error en el registro de asignación de códigos patrimoniales a sede."
						);
					}*/
				}

			}
			

			$this->session->set_userdata($session_var_name, $session_var_data);
		}

		$this->load->view("template_view", $vars);
	}
	
	public function asignacion_reporte_xls(){

		$session_var_name = "asignacion_data";
		

		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Content-type: application/vnd.ms-excel");
		$this->output->set_header("Content-Disposition: attachment; filename=\"REPORTE PREVIO A ASIGNACION DE INVENTARIO.xls\"");
		

		$session_var_data = array(
			"sede" => array(),
			"cargo" => array(),
			"inventario" => array()
		);
		
		if($this->session->userdata($session_var_name)){
			$session_var_data = $this->session->userdata($session_var_name);
		}
		
		$vars = array(
			"title" => "REPORTE PRELIMINAR DE ASIGNACIÓN DE ITEMS DE INVENTARIO A SEDES",
			"columns" => array("CODIGO", "DESCRIPCION", "SEDE", "CARGO"),
			"rows" => array()
		);
		
		foreach($session_var_data["inventario"] as $key => $value){
			$vars["rows"][] = array(
				"CODIGO" => $value["inventario_codigo"],
				"DESCRIPCION" => $value["inventario_descripcion"],
				"SEDE" => $value["sede_nombre"],
				"CARGO" => $value["cargo_nombre"],
			);
		}
		
		$this->load->view("template_xls", $vars);
	}
	
	public function asignacion_reporte(){
		$vars = array(
			"title" => "Asociación sede-ítem",
			"view" => "template_crud"
		);
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();//util para solo reportes
		//$crud->set_theme("bootstrap");
		//$crud->set_language("spanish");
		$crud->set_table("inventario");
		$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha", "asignado_sede_id", "asignado_cargo_id", "asignado_fecha");
		$crud->fields("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha", "asignado_sede_id", "asignado_cargo_id", "asignado_fecha");
		//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
		$crud->unset_texteditor("inventario_observacion", "full_text");
		$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
		$crud->set_relation("asignado_cargo_id", "cargo", "cargo_nombre");
		//$crud->callback_before_insert(array($this, "inventario____before_insert"));
		$crud->display_as(array(
			"inventario_codigo" => "CODIGO",
			"inventario_descripcion" => "DESCRIPCION",
			"inventario_observacion" => "OBSERVACION",
			"inventario_fecha" => "FECHA_REGISTRADO",
			"asignado_sede_id" => "SEDE",
			"asignado_cargo_id" => "CARGO",
			"asignado_fecha" => "FECHA_ASIGNADO"
		));
		$crud->field_type("inventario_descripcion", "dropdown", array(
			"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
			"Lector de código de barras TC10" => "Lector de código de barras TC10"
		));
		//$crud->field_type("m0_registrado_fecha", "invisible");
		$crud->set_subject("asignacion de items a sedes");//nombre que se mostrara, ejm: "Agregar usuario"
		
		$crud->unset_read();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		//$crud->unset_operations()
		
		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		
		$this->load->view("template_view", $vars);
	}
	

	
	public function sede_recepcion_reporte(){
		$varname_user_data = "sede_recepcion_reporte_data";
		
		$vars = array(
			"title" => "Recepción de ítems en todas las sedes",
			"view" => "secudra/sede_recepcion_reporte_view"
		);
		

		$vars["data"]["sedes"] = $this->model->sedes();
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		

		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
				}else{
					$varvalue_user_data["sede"] = array(
						"sede_id" => "-1",
						"sede_nombre" => ""
					);
					/*$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);*/
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();//util para solo reportes
		//$crud->set_theme("bootstrap");
		//$crud->set_language("spanish");
		$crud->set_table("inventario");
		$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha",
		  "asignado_sede_id", "asignado_fecha", "recibido_por_usuario_id", "recibido_observacion", "recibido_fecha", "estado");
		//$crud->fields("inventario_codigo", "inventario_descripcion", "m0_registrado_observacion", "m0_registrado_fecha");
		//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
		//$crud->unset_texteditor("m0_registrado_observacion", "full_text");


		$crud->field_type('estado','dropdown',array('2'=>'Operativo','1'=>'Inoperativo'));
		
		if(isset($varvalue_user_data["sede"]["sede_id"]) && $varvalue_user_data["sede"]["sede_id"] != "-1"){
			$crud->where("asignado_sede_id", $varvalue_user_data["sede"]["sede_id"]);
		}
		
		//$crud->or_where("asignado_sede_id", "21");
		$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
		$crud->set_relation("recibido_por_usuario_id", "usuario", "usuario_username");
		$crud->display_as(array(
			"inventario_codigo" => "CODIGO",
			"inventario_descripcion" => "DESCRIPCION",
			"inventario_observacion" => "OBSERVACION",
			"inventario_fecha" => "FECHA",
			
			"asignado_sede_id" => "ASIGNADO_SEDE",
			"asignado_fecha" => "ASIGNADO_FECHA",
			"recibido_por_usuario_id" => "RECIBIDO_USUARIO",
			"recibido_observacion" => "RECIBIDO_OBSERVACION",
			"recibido_fecha" => "RECIBIDO_FECHA"
		));
		/*$crud->field_type("inventario_descripcion", "dropdown", array(
			"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
			"Lector de código de barras TC10" => "Lector de código de barras TC10"
		));*/
		$crud->field_type("inventario_fecha", "invisible");
		$crud->set_subject("inventario");//nombre que se mostrara, ejm: "Agregar usuario"
		
		//$crud->unset_read();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		//$crud->unset_operations();
		

		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		
		
		$this->load->view("template_view", $vars);
	}
}
?>