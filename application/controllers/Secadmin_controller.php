<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Secadmin_controller extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model("Sistema_model", "model");
		$this->load->library('grocery_CRUD');
	}
	
	public function usuario(){
		$vars = array(
			"title" => "Usuarios",
			"view" => "template_crud"
		);
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();
		//$crud->set_language("spanish");
		$crud->set_table("usuario");
		$crud->set_relation("perfil_id", "perfil", "perfil_nombre");
		$crud->set_relation("cargo_id", "cargo", "cargo_nombre");
		$crud->set_subject("usuario");
		
		
		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		
		
		
		$this->load->view("template_view", $vars);
	}
	
	
	
	
	
	// JHV-Inicio
        // ---------- REGISTRAR EQUIPOS POR SEDE
         // JHV-Inicio
        // ---------- REGISTRAR EQUIPOS POR SEDE
        public function registro_equipos_sede() {
            
            // Nombre de la variable en sesion
            $session_var_name = "registra_equipos";
                
            $vars = array(
                "title" => "Registrar equipos por sede",
"view" => "secadmin/registrar_equipos_sede_view"
            );
            $vars["data"]["sedes"] = $this->model->sedes();                
                
            // Consultar todas las sedes
            $session_var_data = array(
"sede" => array()
            );
                
            // leer los datos guardados en session (Cada vez que ejecutamos una accion
            if($this->session->userdata($session_var_name)){
$session_var_data = $this->session->userdata($session_var_name);
            }                
            // ---------- POST
            if($this->input->post("accion")){
if($this->input->post("accion") == "seleccionar-sede"){
                    $sede_id = $this->input->post("sede_id"); 
                    $result = $this->model->sede_por_id($sede_id);
                    $result_nro_operador_informatico = $this->model->sedes_nro_operador_informatico($sede_id);
                    
                    if($result) {
$session_var_data["sede"] = array(
                            "sede_id" => $result["sede_id"],
                            "sede_nombre" => $result["sede_nombre"],
                            "nro_operador_informatico" => $result_nro_operador_informatico["sede_oi"]
);
$vars["message2"] = array(
                            "type" => "success",
                            "text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
); 
                    } else { 
$session_var_data["sede"] = array();
                    }                                
} else if($this->input->post("accion") == "agregar-oi"){ 
                    $sede_id = trim($this->input->post("sede_id"));
                    $oper_informatico = array(
                        'sede_oi' => trim($this->input->post("nro_oper_informatico"))
                    );
                    $result = $this->model->sedes_guardar_nro_operador_informatico($sede_id, $oper_informatico);
                                
                    if($result) {
                        $session_var_data["sede"] = array(
                            "sede_id" => $sede_id,
                            "nro_operador_informatico" => trim($this->input->post("nro_oper_informatico"))
                        );                                    
                        $vars["message2"] = array(
                            "type" => "success",
                            "text" => "Se ha guardado correctamente"
                        );                                    
                    }
} else if($this->input->post("accion") == "mostrar-equipos"){
    
}
                // solo si hay modificaciones se debe actualizar
$this->session->set_userdata($session_var_name, $session_var_data);
            }                
            if(isset($session_var_data["sede"]["sede_id"]) && $session_var_data["sede"]["sede_id"] != "-1") {
$crud = new grocery_CRUD();
$crud->unset_jquery();
//$crud->unset_jquery_ui();
//$crud->unset_operations();//util para solo reportes
//$crud->set_theme("bootstrap");
                
$crud->set_table("inventario");
$crud->columns("inventario_codigo", "inventario_descripcion", "recibido_observacion", "inventario_fecha",
                    "asignado_sede_id", "asignado_fecha", "estado");
$crud->fields("estado");
$crud->unset_texteditor("recibido_observacion", "full_text");
$crud->where("asignado_sede_id", $session_var_data["sede"]["sede_id"]);
$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
                
$crud->display_as(array(
"inventario_codigo" => "CODIGO",
"inventario_descripcion" => "DESCRIPCION",
"inventario_observacion" => "RECIBIDO OBSERVACION",
"inventario_fecha" => "FECHA",
"asignado_sede_id" => "ASIGNADO_SEDE",
"asignado_fecha" => "ASIGNADO_FECHA",
"estado" => "OPERATIVIDAD"
));
                $crud->field_type('estado','dropdown',array('2'=>'Operativo','1'=>'Inoperativo'));
$crud->set_subject("equipo");
//$crud->unset_read();
//$crud->unset_add();
//$crud->unset_edit();
//$crud->unset_delete();
//$crud->unset_operations();
// ---------- CRUD OUTPUT
$crud_out = $crud->render();
foreach($crud_out->css_files as $url){
$vars["css"][] = $url;
}
foreach($crud_out->js_files as $url){
$vars["js"][] = $url;
}
$vars["data"]["crud_output"] = $crud_out->output;
                
            } else {
                
            }
                
            $this->load->view("template_view", $vars);
                
        }
        
        public function equipos_oi_sedes_reporte() {

            $vars = array(
                "title" => "Reporte Equipos OI x Sedes",
"view" => "secadmin/equipos_oi_sedes_reporte_view"
            );
            
            // Sedes
            $sedes = $this->model->sedes();
            
            // Generar reporte       
            $arraySedes = array();
            $itemSede = array();
            
            foreach($sedes as $row) {
                //Sede
                $itemSede['sede_nombre'] = $row['sede_nombre'];   
                //Nro OI
                $itemSede['sede_oi'] = $row['sede_oi']; 
                // Tablets
                $tableta = "TABLET YOGA 8.2";
                $total_tabletas_x_sede = $this->model->obtener_total_productos_x_sede($row["sede_id"], $tableta);
                $itemSede['tablets'] = $total_tabletas_x_sede['total'];
                // Lectoras
                $lectora = "LECTORA DE CODIGO DE BARRA BARCODE CT10X";
                $total_lectoras_x_sede = $this->model->obtener_total_productos_x_sede($row["sede_id"], $lectora); 
                $itemSede['lectoras'] = $total_lectoras_x_sede['total'];
                // Tablet y Lectora
                if ($total_tabletas_x_sede['total'] < $total_lectoras_x_sede['total']) {
                    $menor = $total_tabletas_x_sede['total'];
                } else {
                    $menor = $total_lectoras_x_sede['total'];
                }
                $itemSede['tablet_lectora'] = $menor;
                // Tablet Inoperativa
                $total_tabletas_inoperativos = $this->model->obtener_productos_inoperativos_sede($row["sede_id"], $tableta);
                $itemSede['tablets_inoperativos'] = $total_tabletas_inoperativos['inoperativo'];                
                // Lectora Inoperativa
                $total_lectoras_inoperativos = $this->model->obtener_productos_inoperativos_sede($row["sede_id"], $lectora);
                $itemSede['letoras_inoperativos'] = $total_lectoras_inoperativos['inoperativo'];                
                // Tablet Operativo
                $tabletOperativo = $itemSede['tablets'] - $itemSede['tablets_inoperativos'];
                // Lectora Operativo
                $lectoraOperativo = $itemSede['lectoras'] - $itemSede['letoras_inoperativos'];
                // Tablet Lectora Operativo
                if($tabletOperativo < $lectoraOperativo) {
                    $tabletLectoraOperativo = $tabletOperativo;
                } else {
                    $tabletLectoraOperativo = $lectoraOperativo;
                }                 
                $itemSede['tablet_lectora_operativo'] = $tabletLectoraOperativo;
                
                // Excedente Tablets
                $itemSede['excedente_tablets'] = $itemSede['tablets'] -  $itemSede['sede_oi'] - $itemSede['tablets_inoperativos'];
                
                // Excedente Lectoras
                $itemSede['excedente_lectoras'] = $itemSede['lectoras'] -  $itemSede['sede_oi'] - $itemSede['letoras_inoperativos'];
                
                // Tablet Lectora Excedente
                if($itemSede['excedente_tablets'] < $itemSede['excedente_lectoras']) {
                    $disponibleTabletLectora = $itemSede['excedente_tablets'];
                } else {
                    $disponibleTabletLectora = $itemSede['excedente_lectoras'];
                }                
                $itemSede['diponibles_tablet_lectora'] = $disponibleTabletLectora;
                // Disponible Tablets
                $itemSede['diponibles_tablets'] = $itemSede['excedente_tablets'] - $itemSede['diponibles_tablet_lectora'];                
                // Disponible Lecotras
                $itemSede['diponibles_lectoras'] = $itemSede['excedente_lectoras'] - $itemSede['diponibles_tablet_lectora'];

                array_push($arraySedes,$itemSede);
            }       
            $vars["data"]["total_sedes"] = $arraySedes;
           
            $this->load->view("template_view", $vars);
        }
        // JHV-Fin
}
?>