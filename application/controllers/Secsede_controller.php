<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Secsede_controller extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model("Sistema_model", "model");
		$this->load->library('grocery_CRUD');
	}
	
	public function index(){
		
	}
	
	public function recepcion(){
		$varname_user_data = "recepcion_data";
		
		$vars = array(
			"title" => "Registrar ítems recibidos en la sede",
			"view" => "secsede/recepcion_view"
		);
		
		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			),
			"inventario" => array(
				/*"inventario_id" => "",
				"inventario_codigo" => "",
				"inventario_descripcion" => "",
				"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		

		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
					$varvalue_user_data["inventario"] = array();

				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}else if($this->input->post("accion") == "consultar-codigo"){
				$codigo = $this->input->post("codigo");
				$result = $this->model->inventario_recepcion_por_codigo($codigo);

				if(count($result) > 0){
					if($result["sede_id"] != ""){
						if($result["sede_id"] == $varvalue_user_data["sede"]["sede_id"]){
							$varvalue_user_data["inventario"] = array(
								"inventario_id" => $result["inventario_id"],
								"inventario_codigo" => $result["inventario_codigo"],
								"inventario_descripcion" => $result["inventario_descripcion"],
								"inventario_observacion" => $result["inventario_observacion"],
								"recibido_observacion" => $result["recibido_observacion"],
								"sede_id" => $result["sede_id"],
								"sede_nombre" => $result["sede_nombre"]
							);
							
							/*
							if($result["recibido_por_usuario_id"] == ""){
								
							}else{
								$vars["message2"] = array(
									"type" => "warning",
									"text" => "El código \"".$codigo."\" ya fue recepcionado el \"".$result["recibido_fecha"]."\"."
								);
							}*/
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "El código \"".$codigo."\" está asignado a la sede \"".$result["sede_nombre"]."\"."
							);
						}
					}else{
						$vars["message2"] = array(
							"type" => "warning",
							"text" => "El código \"".$codigo."\" no está asignado a ninguna sede."
						);
					}
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Código no encontrado en al base de datos."
					);
				}
				
				
			
			
			}else if($this->input->post("accion") == "confirmar-recepcion"){
				$codigo = $this->input->post("codigo");
				$sede_id = $this->input->post("sede_id");
				$observacion = $this->input->post("observacion");
				
				if($this->model->inventario_registrar_recepcion($codigo, $sede_id, $login["usuario_id"], $observacion)){
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha registrado el item correctamente."
					);
					$varvalue_user_data["inventario"] = array();//limpiar la variable
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "No se pudo completar el registro de recepción de items."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		$this->load->view("template_view", $vars);
	}
	
	public function recepcion_reporte(){
		$varname_user_data = "recepcion_reporte_data";
		
		$vars = array(
			"title" => "Ítems recibidos en la sede",
			"view" => "secsede/recepcion_reporte_view"
		);
		

		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		
		
		if(isset($varvalue_user_data["sede"]["sede_id"])){
			$crud = new grocery_CRUD();
			//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
			$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			//$crud->unset_operations();//util para solo reportes
			//$crud->set_language("spanish");
			$crud->set_table("inventario");
			$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha",
			  "asignado_sede_id", "asignado_fecha", "recibido_por_usuario_id", "recibido_observacion", "recibido_fecha");
			//$crud->fields("inventario_codigo", "inventario_descripcion", "m0_registrado_observacion", "m0_registrado_fecha");
			//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
			//$crud->unset_texteditor("m0_registrado_observacion", "full_text");
			$crud->where("asignado_sede_id", $varvalue_user_data["sede"]["sede_id"]);
			//$crud->or_where("asignado_sede_id", "21");
			$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
			$crud->set_relation("recibido_por_usuario_id", "usuario", "usuario_username");
			$crud->display_as(array(
				"inventario_codigo" => "CODIGO",
				"inventario_descripcion" => "DESCRIPCION",
				"inventario_observacion" => "OBSERVACION",
				"inventario_fecha" => "FECHA",
				
				"asignado_sede_id" => "ASIGNADO_SEDE",
				"asignado_fecha" => "ASIGNADO_FECHA",
				"recibido_por_usuario_id" => "RECIBIDO_USUARIO",
				"recibido_observacion" => "RECIBIDO_OBSERVACION",
				"recibido_fecha" => "RECIBIDO_FECHA"
			));
			/*$crud->field_type("inventario_descripcion", "dropdown", array(
				"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
				"Lector de código de barras TC10" => "Lector de código de barras TC10"
			));*/
			$crud->field_type("inventario_fecha", "invisible");
			$crud->set_subject("inventario");
			
			$crud->unset_read();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			//$crud->unset_operations();
			
			$crud_out = $crud->render();
			foreach($crud_out->css_files as $url){
				$vars["css"][] = $url;
			}
			foreach($crud_out->js_files as $url){
				$vars["js"][] = $url;
			}
			$vars["data"]["crud_output"] = $crud_out->output;
		}
		
		
		$this->load->view("template_view", $vars);
	}

	public function retorno_defecto(){
		$varname_user_data = "retorno_defecto_data";
		
		$vars = array(
			"title" => "Registro de devolución de ítems por fallas",
			"view" => "secsede/retorno_defecto_view"
		);
		
		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);
		//print_r($vars["data"]["sedes"]);
		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		

		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
					$varvalue_user_data["inventario"] = array();// limpiar la variable

				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}else if($this->input->post("accion") == "consultar-codigo"){
				$codigo = $this->input->post("codigo");
				$result = $this->model->inventario_retorno_por_codigo($codigo);
				//print_r($result);
				if(count($result) > 0){
					if($result["sede_id"] != ""){
						if($result["sede_id"] == $varvalue_user_data["sede"]["sede_id"]){
							if($result["retorno_fecha"] == ""){
								$varvalue_user_data["inventario"] = array(
									"inventario_id" => $result["inventario_id"],
									"inventario_codigo" => $result["inventario_codigo"],
									"inventario_descripcion" => $result["inventario_descripcion"],
									"inventario_observacion" => $result["inventario_observacion"],
									"sede_id" => $result["sede_id"],
									"sede_nombre" => $result["sede_nombre"],
									"retorno_observacion" => $result["retorno_observacion"],
									"retorno_fecha" => $result["retorno_fecha"]
								);
							}else{
								$vars["message2"] = array(
									"type" => "warning",
									"text" => "El código \"".$codigo."\" ya fue retornado el \"".$result["retorno_fecha"]."\"."
								);
							}
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "El código \"".$codigo."\" está asignado a la sede \"".$result["sede_nombre"]."\"."
							);
						}
					}else{
						$vars["message2"] = array(
							"type" => "warning",
							"text" => "El código \"".$codigo."\" no está asignado a ninguna sede."
						);
					}
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Código no encontrado en al base de datos."
					);
				}
			}else if($this->input->post("accion") == "registrar-retorno"){
				$codigo = $this->input->post("codigo");
				$sede_id = $this->input->post("sede_id");
				$observacion = $this->input->post("observacion");
				
				if($this->model->inventario_retorno_defecto_registrar($codigo, $sede_id, $login["usuario_id"], $observacion)){
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha guardado el registro de retorno por defecto correctamente."
					);
					$varvalue_user_data["inventario"] = array();//limpiar la variable
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "No se pudo completar el registro de retorno por defecto de item."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		$this->load->view("template_view", $vars);
	
	}
	
	public function retorno_defecto_reporte(){
		$varname_user_data = "retorno_defecto_reporte_data";
		
		$vars = array(
			"title" => "Ítems devueltos por fallas",
			"view" => "secsede/retorno_defecto_reporte_view"
		);
		

		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		

		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		
		
		if(isset($varvalue_user_data["sede"]["sede_id"])){
			$crud = new grocery_CRUD();
			//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
			$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			//$crud->unset_operations();//util para solo reportes
			//$crud->set_theme("bootstrap");
			//$crud->set_language("spanish");
			$crud->set_table("inventario");
			$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha",
			 "asignado_sede_id",
	
			 "retorno_por_usuario_id",
			 "retorno_observacion",
			 "retorno_defecto",
			 "retorno_fecha");
			 // "m1_entregado_observacion", "m1_entregado_fecha", "m1_recibido_por",		 "m1_recibido_observacion",		 "m1_recibido_fecha",
			$crud->where("asignado_sede_id", $varvalue_user_data["sede"]["sede_id"]);
			$crud->where("retorno_defecto", "1");
			//$crud->fields("inventario_codigo", "inventario_descripcion", "m0_registrado_observacion", "m0_registrado_fecha");
			//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
			//$crud->unset_texteditor("m0_registrado_observacion", "full_text");
			$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
			$crud->set_relation("retorno_por_usuario_id", "usuario", "usuario_username");
			$crud->display_as(array(
				"inventario_codigo" => "CODIGO",
				"inventario_descripcion" => "REGISTRO_DESCRIPCION",
				"inventarioo_observacion" => "REGISTRO_OBSERVACION",
				"inventario_fecha" => "REGISTRO_FECHA",
				
				"asignado_sede_id" => "ASIGNADO_SEDE",
				"asignado_observacion" => "ASIGNADO_OBSERVACION",
				"asignado_fecha" => "ASIGNADO_FECHA",
				"recibido_por_usuario_id" => "RECIBIDO_USUARIO",
				"recibido_observacion" => "RECIBIDO_OBSERVACION",
				"recibido_fecha" => "RECIBIDO_FECHA",
				
				"retorno_por_usuario_id" => "RETORNO_POR",
				"retorno_observacion" => "RETORNO_OBSERVACION",
				"retorno_defecto" => "RETORNO_DEFECTO",
				"retorno_fecha" => "RETORNO_FECHA"
			));
			/*$crud->field_type("inventario_descripcion", "dropdown", array(
				"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
				"Lector de código de barras TC10" => "Lector de código de barras TC10"
			));*/
			$crud->field_type("inventario_fecha", "invisible");
			$crud->set_subject("inventario");//nombre que se mostrara, ejm: "Agregar usuario"
			
			$crud->unset_read();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			//$crud->unset_operations();
			

			$crud_out = $crud->render();
			foreach($crud_out->css_files as $url){
				$vars["css"][] = $url;
			}
			foreach($crud_out->js_files as $url){
				$vars["js"][] = $url;
			}
			$vars["data"]["crud_output"] = $crud_out->output;
		}
		
		
		$this->load->view("template_view", $vars);
	}
	
	public function retorno(){
		$varname_user_data = "retorno_data";
		
		$vars = array(
			"title" => "Registrar devolución de ítems",
			"view" => "secsede/retorno_view"
		);
		
		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			),
			"inventario" => array(
				/*"inventario_id" => "",
				"inventario_codigo" => "",
				"inventario_descripcion" => "",
				"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
					$varvalue_user_data["inventario"] = array();// limpiar la variable

				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}else if($this->input->post("accion") == "consultar-codigo"){
				$codigo = $this->input->post("codigo");
				$result = $this->model->inventario_retorno_por_codigo($codigo);
				//print_r($result);
				if(count($result) > 0){
					if($result["sede_id"] != ""){
						if($result["sede_id"] == $varvalue_user_data["sede"]["sede_id"]){
							if($result["retorno_fecha"] == ""){
								$varvalue_user_data["inventario"] = array(
									"inventario_id" => $result["inventario_id"],
									"inventario_codigo" => $result["inventario_codigo"],
									"inventario_descripcion" => $result["inventario_descripcion"],
									"inventario_observacion" => $result["inventario_observacion"],
									"sede_id" => $result["sede_id"],
									"sede_nombre" => $result["sede_nombre"],
									"retorno_observacion" => $result["retorno_observacion"],
									"retorno_fecha" => $result["retorno_fecha"]
								);
							}else{
								$vars["message2"] = array(
									"type" => "warning",
									"text" => "El código \"".$codigo."\" ya fue retornado el \"".$result["retorno_fecha"]."\"."
								);
							}
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "El código \"".$codigo."\" está asignado a la sede \"".$result["sede_nombre"]."\"."
							);
						}
					}else{
						$vars["message2"] = array(
							"type" => "warning",
							"text" => "El código \"".$codigo."\" no está asignado a ninguna sede."
						);
					}
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Código no encontrado en al base de datos."
					);
				}
			}else if($this->input->post("accion") == "registrar-retorno"){
				$codigo = $this->input->post("codigo");
				$sede_id = $this->input->post("sede_id");
				$observacion = $this->input->post("observacion");
				
				if($this->model->inventario_retorno_registrar($codigo, $sede_id, $login["usuario_id"], $observacion)){
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha guardado el registro de retorno correctamente."
					);
					$varvalue_user_data["inventario"] = array();//limpiar la variable
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "No se pudo completar el registro de retorno de item."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		$this->load->view("template_view", $vars);
	}
	
	public function retorno_reporte(){
		$varname_user_data = "retorno_reporte_data";
		
		$vars = array(
			"title" => "ítems devueltos",
			"view" => "secsede/retorno_reporte_view"
		);
		

		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		

		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		
		
		if(isset($varvalue_user_data["sede"]["sede_id"])){
			$crud = new grocery_CRUD();
			//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
			$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			//$crud->unset_operations();//util para solo reportes
			//$crud->set_theme("bootstrap");
			//$crud->set_language("spanish");
			$crud->set_table("inventario");
			$crud->columns("inventario_codigo", "inventario_descripcion", "inventario_observacion", "inventario_fecha",
			 "asignado_sede_id",
	
			 "retorno_por_usuario_id",
			 "retorno_observacion",
			 "retorno_defecto",
			 "retorno_fecha");
			 // "m1_entregado_observacion", "m1_entregado_fecha", "m1_recibido_por",		 "m1_recibido_observacion",		 "m1_recibido_fecha",
			$crud->where("asignado_sede_id", $varvalue_user_data["sede"]["sede_id"]);
			$crud->where("retorno_defecto", "0");
			//$crud->fields("inventario_codigo", "inventario_descripcion", "m0_registrado_observacion", "m0_registrado_fecha");
			//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
			//$crud->unset_texteditor("m0_registrado_observacion", "full_text");
			$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
			$crud->set_relation("retorno_por_usuario_id", "usuario", "usuario_username");
			$crud->display_as(array(
				"inventario_codigo" => "CODIGO",
				"inventario_descripcion" => "REGISTRO_DESCRIPCION",
				"inventarioo_observacion" => "REGISTRO_OBSERVACION",
				"inventario_fecha" => "REGISTRO_FECHA",
				
				"asignado_sede_id" => "ASIGNADO_SEDE",
				"asignado_observacion" => "ASIGNADO_OBSERVACION",
				"asignado_fecha" => "ASIGNADO_FECHA",
				"recibido_por_usuario_id" => "RECIBIDO_USUARIO",
				"recibido_observacion" => "RECIBIDO_OBSERVACION",
				"recibido_fecha" => "RECIBIDO_FECHA",
				
				"retorno_por_usuario_id" => "RETORNO_POR",
				"retorno_observacion" => "RETORNO_OBSERVACION",
				"retorno_defecto" => "RETORNO_DEFECTO",
				"retorno_fecha" => "RETORNO_FECHA"
			));
			/*$crud->field_type("inventario_descripcion", "dropdown", array(
				"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
				"Lector de código de barras TC10" => "Lector de código de barras TC10"
			));*/
			$crud->field_type("inventario_fecha", "invisible");
			$crud->set_subject("inventario");//nombre que se mostrara, ejm: "Agregar usuario"
			
			$crud->unset_read();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			//$crud->unset_operations();
			

			$crud_out = $crud->render();
			foreach($crud_out->css_files as $url){
				$vars["css"][] = $url;
			}
			foreach($crud_out->js_files as $url){
				$vars["js"][] = $url;
			}
			$vars["data"]["crud_output"] = $crud_out->output;
		}
		
		$this->load->view("template_view", $vars);
	}

	public function entregado_personal(){
		$varname_user_data = "entregado_personal_data";
		
		$vars = array(
			"title" => "Registrar ítems entregados al personal",
			"view" => "secsede/entregado_personal_view"
		);
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			),
			"personal" => array(
				/*personal_id => "",
				personal_dni,
				personal_nombre
				*/
			),
			"items" => array(
			/*
				"00001" => array(
					"inventario_id" => "",
					"inventario_codigo" => "",
					"inventario_descripcion" => "",
					"sede_id" => "",
					"sede_nombre" => "",
					"personal_id" => "",
					"personal_dni" => "",
					"personal_nombre" => ""
				)
			*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);
		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
					$varvalue_user_data["personal"] = array();
				}else{
					$varvalue_user_data["sede"] = array();
				}
			}else if($this->input->post("accion") == "seleccionar-personal"){
				//$sede_id = $varvalue_user_data["sede"]["sede_id"]
				$personal_id = $this->input->post("personal_id");
				
				$result = $this->model->personal_por_id($personal_id);
				if($result){
					$varvalue_user_data["personal"] = array(
						"personal_id" => $result["personal_id"],
						"personal_dni" => $result["personal_dni"],
						"personal_nombre" => $result["personal_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado el personal \"".$result["personal_nombre"]."\"."
					);
				}else{
					$varvalue_user_data["personal"] = array();
				}
			}else if($this->input->post("accion") == "eliminar-item"){
				$codigo = $this->input->post("codigo");
				if(array_key_exists($codigo, $varvalue_user_data["items"])){
					unset($varvalue_user_data["items"][$codigo]);

					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha eliminado el código \"".$codigo."\" satisfactoriamente."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "El código \"".$codigo."\" no se encuentra en el listado temporal."
					);
				}
			}else if($this->input->post("accion") == "agregar-item"){
				$codigo = trim($this->input->post("codigo"));
				if(!array_key_exists($codigo, $varvalue_user_data["items"])){
					$r1 = $this->model->personal_por_id($varvalue_user_data["personal"]["personal_id"]);
					$r2 = $this->model->entrega_item_inventario_por_codigo($codigo);
					
					if(count($r2) == 1){
						if($r2[0]["sede_id"] == $varvalue_user_data["sede"]["sede_id"]){
							if($r2[0]["personal_id"] == ""){
								$varvalue_user_data["items"][$codigo] = array(
									"inventario_id" => $r2[0]["inventario_id"],
									"inventario_codigo" => $r2[0]["inventario_codigo"],
									"inventario_descripcion" => $r2[0]["inventario_descripcion"],
									"sede_id" => $r2[0]["sede_id"],
									"sede_nombre" => $r2[0]["sede_nombre"],
									"personal_id" => $r1["personal_id"],
									"personal_dni" => $r1["personal_dni"],
									"personal_nombre" => $r1["personal_nombre"]
								);
								
								/*$vars["message2"] = array(
									"type" => "success",
									"text" => "El código \"".$codigo."\" se agregó satisfactoriamente."
								);*/
							}else{
								$vars["message2"] = array(
									"type" => "warning",
									"text" => "El código \"".$codigo."\" (".$r2[0]["inventario_descripcion"].") ya se ha entregado al personal ".$r1["personal_nombre"]."."
								);
							}
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "El código \"".$codigo."\" (".$r2[0]["inventario_descripcion"].") se ha asignado a la sede \"".$r2[0]["sede_nombre"]."\"."
							);
						}
					}else{
						$vars["message2"] = array(
							"type" => "danger",
							"text" => "El código \"".$codigo."\" no se encuentra en la base de datos."
						);
					}
				}else{
					$vars["message2"] = array(
						"type" => "warning",
						"text" => "El código \"".$codigo."\" se encuentra en la vista preliminar y está asignado a ".$varvalue_user_data["items"][$codigo]["personal_nombre"]."."
					);
					
				}
			
			}else if($this->input->post("accion") == "terminar-y-guardar"){
				if(count($varvalue_user_data["items"]) > 0){
					$bien = array();
					$mal = array();
					foreach($varvalue_user_data["items"] as $key => $value){
						if($this->model->entregado_personal_registrar($value["sede_id"], $value["personal_id"], $value["inventario_codigo"])){
							$bien[] = $value["inventario_codigo"];
						}else{
							$mal[] = $value["inventario_codigo"];
						}
					}
					
					if(count($bien) > 0){
						if(count($mal) == 0){
							$vars["message2"] = array(
								"type" => "success",
								"text" => "Los siguientes items se han entregado correctamente: [".implode(",", $bien)."]."
							);
						}else{
							$vars["message2"] = array(
								"type" => "warning",
								"text" => "Los siguientes items se han asignado correctamente: [".implode(",", $bien)."], sin embargo los siguientes items presentaron un error: [".implode(",", $mal)."]."
							);
						}
						$varvalue_user_data["items"] = array();
					}else{
						$vars["message2"] = array(
							"type" => "danger",
							"text" => "No se ha podido asignar ninguno de los items."
						);
					}
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		

		if(isset($varvalue_user_data["sede"]["sede_id"])){
			$vars["data"]["personal"] = $this->model->personal_por_sede($varvalue_user_data["sede"]["sede_id"]);
		}
		
		$this->load->view("template_view", $vars);
	}
	
	
	public function entregado_personal_reporte_xls(){

		$varname_user_data = "entregado_personal_data";
		

		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Content-type: application/vnd.ms-excel");
		$this->output->set_header("Content-Disposition: attachment; filename=\"REPORTE DE ENTREGA DE ITEMS AL PERSONAL INFORMATICO.xls\"");
		

		$varvalue_user_data = array(
			"items" => array()
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		$vars = array(
			"title" => "REPORTE PRELIMINAR DE ENTREGA DE ITEMS AL PERSONAL INFORMÁTICO",
			"columns" => array("SEDE", "DNI", "PERSONAL", "ITEM", "DESCRIPCION"),
			"rows" => array()
		);
		
		foreach($varvalue_user_data["items"] as $key => $value){
			$vars["rows"][] = array(
				"SEDE" => $value["sede_nombre"],
				"DNI" => $value["personal_dni"],
				"PERSONAL" => $value["personal_nombre"],
				"ITEM" => $value["inventario_codigo"],
				"DESCRIPCION" => $value["inventario_descripcion"]
			);
		}
		
		$this->load->view("template_xls", $vars);
	}
	
	public function entregado_personal_reporte(){
		
		$varname_user_data = "entregado_personal_reporte_data";
		
		$vars = array(
			"title" => "Ítems entregados al personal",
			"view" => "secsede/entregado_personal_reporte_view"
		);
		

		$login = $this->session->userdata("login_data");
		$vars["data"]["sedes"] = $this->model->sedes_por_usuario($login["usuario_id"]);

		if(count($vars["data"]["sedes"]) == 0){
			$vars["message2"] = array(
				"type" => "warning",
				"text" => "Usted no cuenta con registros de sede asignados a su usuario."
			);
		}
		
		$varvalue_user_data = array(
			"sede" => array(
				/*"sede_id" => "",
				"sede_nombre" => ""*/
			)
		);
		
		if($this->session->userdata($varname_user_data)){
			$varvalue_user_data = $this->session->userdata($varname_user_data);
		}
		
		
		if($this->input->post("accion")){
			if($this->input->post("accion") == "seleccionar-sede"){
				$sede_id = $this->input->post("sede_id");
				
				$result = $this->model->sede_por_id($sede_id);
				if($result){
					$varvalue_user_data["sede"] = array(
						"sede_id" => $result["sede_id"],
						"sede_nombre" => $result["sede_nombre"]
					);
					$vars["message2"] = array(
						"type" => "success",
						"text" => "Se ha seleccionado la sede \"".$result["sede_nombre"]."\"."
					);
				}else{
					$vars["message2"] = array(
						"type" => "danger",
						"text" => "Ocurrio un error al intentar seleccionar la sede."
					);
				}
			}
			$this->session->set_userdata($varname_user_data, $varvalue_user_data);
		}
		
		
		if(isset($varvalue_user_data["sede"]["sede_id"])){
			$crud = new grocery_CRUD();
			//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
			$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			//$crud->unset_operations();//util para solo reportes
			//$crud->set_language("spanish");
			$crud->set_table("inventario");
			
			
			$crud->columns("inventario_codigo", "inventario_descripcion", "asignado_sede_id", "recibido_por_usuario_id", "distribuido_a_personal_id", "distribuido_fecha");
			//$crud->fields("inventario_codigo", "inventario_descripcion", "m0_registrado_observacion", "m0_registrado_fecha");
			//$crud->field_type("m0_registrado_fecha", "hidden", date("Y-m-d H:i:s"));// pero esto ocurre para ingresar y actualizar
			//$crud->unset_texteditor("m0_registrado_observacion", "full_text");
			$crud->where("asignado_sede_id", $varvalue_user_data["sede"]["sede_id"]);
			//$crud->or_where("asignado_sede_id", "21");
			$crud->set_relation("asignado_sede_id", "sede", "sede_nombre");
			$crud->set_relation("recibido_por_usuario_id", "usuario", "usuario_username");
			$crud->set_relation("distribuido_a_personal_id", "personal", "personal_nombre");
			
			$crud->display_as(array(
				"inventario_codigo" => "CODIGO",
				"inventario_descripcion" => "DESCRIPCION",
				
				"asignado_sede_id" => "SEDE",
				"recibido_por_usuario_id" => "USUARIO",
				"distribuido_a_personal_id" => "PERSONAL",
				"distribuido_fecha" => "FECHA"
			));
			/*$crud->field_type("inventario_descripcion", "dropdown", array(
				"Tablet Lenovo Yoga 2" => "Tablet Lenovo Yoga 2",
				"Lector de código de barras TC10" => "Lector de código de barras TC10"
			));*/
			//$crud->field_type("inventario_fecha", "invisible");
			$crud->set_subject("inventario");
			
			$crud->unset_read();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			//$crud->unset_operations();
			
			$crud_out = $crud->render();
			foreach($crud_out->css_files as $url){
				$vars["css"][] = $url;
			}
			foreach($crud_out->js_files as $url){
				$vars["js"][] = $url;
			}
			$vars["data"]["crud_output"] = $crud_out->output;
		}
		
		
		$this->load->view("template_view", $vars);
	
	}
	
}
?>