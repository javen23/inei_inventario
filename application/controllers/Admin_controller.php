<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		//$this->load->model("Sistema_model", "model");
		$this->load->library('grocery_CRUD');
	}
	
	// ---------- USUARIOS
	public function usuario(){
		$vars = array(
			"title" => "Usuarios",
			"view" => "template_crud"
		);
		
		$crud = new grocery_CRUD();
		//$crud->set_crud_url_path(base_url("Admin_controller/usuario"));
		$crud->unset_jquery();
		//$crud->unset_jquery_ui();
		//$crud->unset_operations();//util para solo reportes
		//$crud->set_theme("bootstrap");
		//$crud->set_language("spanish");
		$crud->set_table("usuario");
		$crud->set_relation("perfil_id", "perfil", "perfil_nombre");
		$crud->set_relation("cargo_id", "cargo", "cargo_nombre");
		$crud->set_subject("usuario");//nombre que se mostrara, ejm: "Agregar usuario"
		
		
		// ---------- CRUD OUTPUT
		$crud_out = $crud->render();
		foreach($crud_out->css_files as $url){
			$vars["css"][] = $url;
		}
		foreach($crud_out->js_files as $url){
			$vars["js"][] = $url;
		}
		$vars["data"]["crud_output"] = $crud_out->output;
		
		
		
		$this->load->view("template_view", $vars);
	}
}
?>