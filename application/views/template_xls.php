<html>
<head>
<style>
/*body{
	font-family: calibri, san-serif; font-size:9pt;
}
td, th{
	white-space: nowrap;
}*/
.date{
	mso-number-format: "Short Date";
}
.text{
	mso-number-format: "\@";
}
.percent{
	mso-number-format: "Percent"; text-align:right;
}
.int{
	mso-number-format: "\#\,\#\#0"; text-align:right;
}
.account{
	mso-number-format: "\#\,\#\#0;[Red\]\(\#\,\#\#0\);\-"; text-align:right;
}
.money{
	mso-number-format: "Currency"; text-align:right;
}
</style>
</head>
<body>
<?php
if(isset($columns) && isset($rows)){
	if(count($columns) > 0){
		echo "<table border=\"1\"><tr>";
		if(isset($title)){
			echo "<th colspan=\"".count($columns)."\" style=\"background-color:#CCC;\">".htmlentities($title)."</th></tr><tr>";
		}
		for($i = 0; $i < count($columns); $i++){
			echo "<th style=\"background-color:#CCC;\">".htmlentities($columns[$i])."</th>";
		}
		echo "</tr>";
		
		for($i = 0; $i < count($rows); $i++){
			echo "<tr>";
			foreach($columns as $key => $value){
				echo "<td class=\"text\">".htmlentities($rows[$i][$value])."</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}

}else{

}
?>
</body>
</html>