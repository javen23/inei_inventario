<?php
$login = $this->session->userdata("login_data");
?>
<div class="container">
	<div class="jumbotron">
		<h2>Hola, <?php echo $login["usuario_nombre"]; ?>.</h2>
		<p>Bienvenido al Sistema de Inventario Electrónico del Proyecto EDNOM 2016 - INEI. En esta pantalla verás una lista de opciones a las que podrás acceder y una breve descripción de ellas.</p>
	</div>
	
	<div class="row">
	<?php
	switch($login["perfil_id"]){
		case 1://super
		case 2://admin
	?>
	
	<?php
			break;
		case 3://udra admin
		case 4://udra user
	?>
		<?php if($login["perfil_id"] == 3) : ?>
		<div class="col-md-4">
			<h2>Registrar inventario inicial</h2>
			<p>Registre items de inventario en la primera fase de distribución. Consulte información acerca del inventario y haga observaciones antes de asignarlas a las sedes.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secudra/inventario"); ?>" role="button">Ingresar &raquo;</a></p>
			<!--<ul>
				<li><a href="">Reporte de usuarios 1</a></li>
				<li><a href="">Reporte de usuarios 2</a></li>
			</ul>-->
		</div>
		<div class="col-md-4">
			<h2>Asignar items a sedes</h2>
			<p>Asigne items de inventario a cada una de las 35 sedes a nivel nacional.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secudra/asignacion"); ?>" role="button">Ingresar &raquo;</a></p>
		</div>
		<!--<div class="col-md-4">
			<h2>Registrar inventario final</h2>
			<p>Verifique y haga observaciones del inventario devuelto de las sedes a nivel nacional.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secudra/devolucion"); ?>" role="button">Ingresar &raquo;</a></p>
		</div>-->
		<?php endif; ?>
	<?php
			break;
		case 5://sede admin
		case 6://sede user
	?>
		<div class="col-md-4">
			<h2>Recepción de items</h2>
			<p>Registre los items recibidos en la primera fase de distribución. Consulte información acerca de los items asignados a su sede y registre observaciones de los items recibidos.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secsede/recepcion"); ?>" role="button">Ingresar &raquo;</a></p>
			<!--<ul>
				<li><a href="">Reporte de usuarios 1</a></li>
				<li><a href="">Reporte de usuarios 2</a></li>
			</ul>-->
		</div>
		<!--<div class="col-md-4">
			<h2>Distribuir items al personal</h2>
			<p>Asigne items de inventario a su personal.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secsede/distribucion"); ?>" role="button">Ingresar &raquo;</a></p>
		</div>
		<div class="col-md-4">
			<h2>Registro de estado de devolución de items</h2>
			<p>Registre el estado de los items antes de su devolución.</p>
			<p><a class="btn btn-default" href="<?php echo base_url("secsede/devolucion"); ?>" role="button">Ingresar &raquo;</a></p>
		</div>-->

	<?php
			break;
	}
	?>
	</div>
	
</div>