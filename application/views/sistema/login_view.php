<div class="container">
	<form class="form-signin center-block" style="max-width:300px;" action="<?php echo base_url("login"); ?>" method="post" enctype="application/x-www-form-urlencoded">
		<input type="hidden" name="accion" value="login" />
		<h2 class="form-signin-heading">Iniciar sesión</h2>
		<label for="input_username" class="sr-only">Correo electrónico</label>
		<br />
		<input name="username" type="text" id="input_username" class="form-control" placeholder="Nombre de usuario" required autofocus>
		<br />
		<label for="input_password" class="sr-only">Contraseña</label>
		<input name="password" type="password" id="input_password" class="form-control" placeholder="Contraseña" required>
		<br />
		<button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
	</form>
</div>