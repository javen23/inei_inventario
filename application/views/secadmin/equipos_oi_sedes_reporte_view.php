<div class="container">
    
      <div class="row">
        <div class="span12 columns">
            
            <p class="text-left">
                <a href="<?php echo base_url("exportar/excel_equipos_oi_sedes"); ?>" class="btn btn-success" target="_blank">EXPORTAR EXCEL</a>
            </p>
            
          <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="header headerSortDown" >#</th>
                    <th class="header headerSortDown" >SEDE</th>
                    <th class="header headerSortDown" >NRO. OI</th>
                    <th class="header headerSortDown" >TABLETS</th>
                    <th class="header headerSortDown" >LECTORAS</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA</th>
                    <th class="header headerSortDown" >TABLET INOPERATIVA</th>
                    <th class="header headerSortDown" >LECTORA INOPERATIVA</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA OPERATIVAS</th>
                    <th class="header headerSortDown" >TABLETS EXCEDENTE</th> 
                    <th class="header headerSortDown" >LECTORAS EXEDENTE</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA EXCEDENTE</th>
                    <th class="header headerSortDown" >TABLETS RESTANTE</th>
                    <th class="header headerSortDown" >LECTORAS RESTANTE</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sumNroOI = 0;
                $sumTablets = 0;
                $sumLectoras = 0;
                $sumTabletLectoras = 0;
                $sumTabletInoperativa = 0;
                $sumLectoraInoperativa = 0;
                $sumTabletLectoraOperativo = 0;
                $sumExcedenteTablets = 0;
                $sumExcedenteLectoras = 0;
                $sumDisponibleTabletLectora = 0;
                $sumDisponibleTablet = 0;
                $sumDisponibleLectora = 0;
                
                $rojo = '#DF7B87';
                $verde = '#57A15C';
                
                $cont = 1;
                foreach ($data["total_sedes"] as $sedes) {                     
                ?>
                    <tr>
                        <td><?php  echo $cont; ?></td>
                        <td><?php  echo isset($sedes['sede_nombre']) ? $sedes['sede_nombre'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['sede_oi']) ? $sedes['sede_oi'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['tablets']) ? $sedes['tablets'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['lectoras']) ? $sedes['lectoras'] : 0 ; ?></td>
                        <!--td><php  echo isset($sedes['tablet_lectora']) ? $sedes['tablet_lectora'] : 0 ; ></td-->
                        <td bgcolor="<?php echo ($sedes['tablet_lectora'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['tablet_lectora']) ? $sedes['tablet_lectora'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['tablets_inoperativos']) ? $sedes['tablets_inoperativos'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['letoras_inoperativos']) ? $sedes['letoras_inoperativos'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['tablet_lectora_operativo'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['tablet_lectora_operativo']) ? $sedes['tablet_lectora_operativo'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['excedente_tablets']) ? $sedes['excedente_tablets'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['excedente_lectoras']) ? $sedes['excedente_lectoras'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_tablet_lectora'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_tablet_lectora']) ? $sedes['diponibles_tablet_lectora'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_tablets'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_tablets']) ? $sedes['diponibles_tablets'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_lectoras'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_lectoras']) ? $sedes['diponibles_lectoras'] : 0 ; ?></td>
                    </tr> 
                <?php
                    $cont++;
                    $sumNroOI = $sumNroOI + $sedes['sede_oi'];
                    $sumTablets = $sumTablets + $sedes['tablets'];
                    $sumLectoras = $sumLectoras + $sedes['lectoras'];
                    $sumTabletLectoras = $sumTabletLectoras + $sedes['tablet_lectora'];
                    $sumTabletInoperativa = $sumTabletInoperativa + $sedes['tablets_inoperativos'];
                    $sumLectoraInoperativa = $sumLectoraInoperativa + $sedes['letoras_inoperativos'];
                    $sumTabletLectoraOperativo = $sumTabletLectoraOperativo + $sedes['tablet_lectora_operativo'];
                    $sumExcedenteTablets = $sumExcedenteTablets + $sedes['excedente_tablets'];
                    $sumExcedenteLectoras = $sumExcedenteLectoras + $sedes['excedente_lectoras'];
                    $sumDisponibleTabletLectora = $sumDisponibleTabletLectora + $sedes['diponibles_tablet_lectora'];
                    $sumDisponibleTablet = $sumDisponibleTablet + $sedes['diponibles_tablets'];
                    $sumDisponibleLectora = $sumDisponibleLectora + $sedes['diponibles_lectoras'];
                } 
                ?>
                    <tr>
                        <td></td>
                        <td><b>Total:</b></td>
                        <td><b><?php  echo $sumNroOI ; ?></b></td>
                        <td><b><?php  echo $sumTablets ; ?></b></td>
                        <td><b><?php  echo $sumLectoras ; ?></b></td>
                        <td><b><?php  echo $sumTabletLectoras ; ?></b></td>
                        <td><b><?php  echo $sumTabletInoperativa ; ?></b></td>
                        <td><b><?php  echo $sumLectoraInoperativa ; ?></b></td>
                        <td><b><?php  echo $sumTabletLectoraOperativo ; ?></b></td>
                        <td><b><?php  echo $sumExcedenteTablets ; ?></b></td>
                        <td><b><?php  echo $sumExcedenteLectoras ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleTabletLectora ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleTablet ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleLectora ; ?></b></td>
                    </tr>                    
            </tbody>            
          </table>

      </div>
    </div>
    
</div>

