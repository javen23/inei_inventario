<div class="container">
    
      <div class="row">
        <div class="span12 columns">
            <style>
			td{
				vertical-align:middle !important;
			}
			th{
				vertical-align:middle !important;
				text-align:center;

			}
			</style>
            <p class="text-left">
                <a href="<?php echo base_url("exportar/excel_equipos_oi_sedes_v2"); ?>" class="btn btn-success" target="_blank">EXPORTAR EXCEL</a>
            </p>
			<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th rowspan="2">#</th>
				<th rowspan="2">SEDE</th>
				<th rowspan="2">NRO DE OI</th>
				<th colspan="4">TABLETS</th>
				<th colspan="4">LECTORAS</th>
				<th colspan="2">PARES (TABLET + LECTORA)</th>
			</tr>
			<tr>
				<th>CANTIDAD</th>
				<th>INOPERATIVAS</th>
				<th>OPERATIVAS</th>
				<th>EXCEDENTE SEGUN OI</th>
				<th>CANTIDAD</th>
				<th>INOPERATIVAS</th>
				<th>OPERATIVAS</th>
				<th>EXCEDENTE SEGUN OI</th>
				<th>PARES OPERATIVOS</th>
				<th>PARES EXCEDENTES</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$rojo = '#DF7B87';
			$verde = '#57A15C';
			$cont = 1; 
			$t1 = 0;
			$t2 = 0;
			$t3 = 0;
			$t4 = 0;
			$t5 = 0;
			$t6 = 0;
			$t7 = 0;
			$t8 = 0;
			$t9 = 0;
			$t10 = 0;
			$t11 = 0;
			
			foreach($data["total_sedes"] as $sedes){
				$tablets_operativos = (int)$sedes["tablets"] - (int)$sedes["tablets_inoperativos"];
				$tablets_operativos_oi = $tablets_operativos - (int)$sedes["sede_oi"];
				
				
				$lectoras_operativos = (int)$sedes["lectoras"] - (int)$sedes["letoras_inoperativos"];
				$lectoras_operativos_oi = $lectoras_operativos - (int)$sedes["sede_oi"];
				
				$pares = $tablets_operativos < $lectoras_operativos ? $tablets_operativos : $lectoras_operativos;
				$pares_excedente = $pares - (int)$sedes["sede_oi"];
				
				$t1 = $t1 + (int)$sedes["sede_oi"];
				$t2 = $t2 + (int)$sedes["tablets"];
				$t3 = $t3 + (int)$sedes["tablets_inoperativos"];
				$t4 = $t4 + $tablets_operativos;
				$t5 = $t5 + $tablets_operativos_oi;
				$t6 = $t6 + (int)$sedes["lectoras"];
				$t7 = $t7 + (int)$sedes["letoras_inoperativos"];
				$t8 = $t8 + $lectoras_operativos;
				$t9 = $t9 + $lectoras_operativos_oi;
				$t10 = $t10 + $pares;
				$t11 = $t11 + $pares_excedente;
			?>
			<tr>
				<td><?php echo $cont; ?></td>
				<td><?php echo $sedes["sede_nombre"]; ?></td>
				<td><?php echo $sedes["sede_oi"]; ?></td>
				<td><?php echo $sedes["tablets"]; ?></td>
				<td><?php echo $sedes["tablets_inoperativos"]; ?></td>
				<td bgcolor="<?php echo $tablets_operativos > 1 ? $verde : $rojo; ?>"><?php echo $tablets_operativos; ?></td>
				<td><?php echo $tablets_operativos_oi; ?></td>
				<td><?php echo $sedes["lectoras"]; ?></td>
				<td><?php echo $sedes["letoras_inoperativos"]; ?></td>
				<td bgcolor="<?php echo $lectoras_operativos > 1 ? $verde : $rojo; ?>"><?php echo $lectoras_operativos; ?></td>
				<td><?php echo $lectoras_operativos_oi; ?></td>
				<td bgcolor="<?php echo $pares > 1 ? $verde : $rojo; ?>"><?php echo $pares; ?></td>
				<td><?php echo $pares_excedente; ?></td>
			</tr> 
			<?php
				$cont++;
			}
			?>
			</tbody>
			<tfoot>
			<tr>
				<th colspan="2" style="text-align:right;">TOTAL:</th>
				<th><?php echo $t1; ?></th>
				<th><?php echo $t2; ?></th>
				<th><?php echo $t3; ?></th>
				<th><?php echo $t4; ?></th>
				<th><?php echo $t5; ?></th>
				<th><?php echo $t6; ?></th>
				<th><?php echo $t7; ?></th>
				<th><?php echo $t8; ?></th>
				<th><?php echo $t9; ?></th>
				<th><?php echo $t10; ?></th>
				<th><?php echo $t11; ?></th>
			</tr>
			</tfoot>
			</table>
		  
		    
          <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="header headerSortDown" >#</th>
                    <th class="header headerSortDown" >SEDE</th>
                    <th class="header headerSortDown" >NRO. OI</th>
                    <th class="header headerSortDown" >TABLETS</th>
                    <th class="header headerSortDown" >LECTORAS</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA</th>
                    <th class="header headerSortDown" >TABLET INOPERATIVA</th>
                    <th class="header headerSortDown" >LECTORA INOPERATIVA</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA OPERATIVAS</th>
                    <th class="header headerSortDown" >TABLETS EXCEDENTE</th> 
                    <th class="header headerSortDown" >LECTORAS EXEDENTE</th>
                    <th class="header headerSortDown" >TABLET Y LECTORA EXCEDENTE</th>
                    <th class="header headerSortDown" >TABLETS RESTANTE</th>
                    <th class="header headerSortDown" >LECTORAS RESTANTE</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sumNroOI = 0;
                $sumTablets = 0;
                $sumLectoras = 0;
                $sumTabletLectoras = 0;
                $sumTabletInoperativa = 0;
                $sumLectoraInoperativa = 0;
                $sumTabletLectoraOperativo = 0;
                $sumExcedenteTablets = 0;
                $sumExcedenteLectoras = 0;
                $sumDisponibleTabletLectora = 0;
                $sumDisponibleTablet = 0;
                $sumDisponibleLectora = 0;
                
                
                
                $cont = 1;
                foreach ($data["total_sedes"] as $sedes) {                     
                ?>
                    <tr>
                        <td><?php  echo $cont; ?></td>
                        <td><?php  echo isset($sedes['sede_nombre']) ? $sedes['sede_nombre'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['sede_oi']) ? $sedes['sede_oi'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['tablets']) ? $sedes['tablets'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['lectoras']) ? $sedes['lectoras'] : 0 ; ?></td>
                        <!--td><php  echo isset($sedes['tablet_lectora']) ? $sedes['tablet_lectora'] : 0 ; ></td-->
                        <td bgcolor="<?php echo ($sedes['tablet_lectora'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['tablet_lectora']) ? $sedes['tablet_lectora'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['tablets_inoperativos']) ? $sedes['tablets_inoperativos'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['letoras_inoperativos']) ? $sedes['letoras_inoperativos'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['tablet_lectora_operativo'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['tablet_lectora_operativo']) ? $sedes['tablet_lectora_operativo'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['excedente_tablets']) ? $sedes['excedente_tablets'] : 0 ; ?></td>
                        <td><?php  echo isset($sedes['excedente_lectoras']) ? $sedes['excedente_lectoras'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_tablet_lectora'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_tablet_lectora']) ? $sedes['diponibles_tablet_lectora'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_tablets'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_tablets']) ? $sedes['diponibles_tablets'] : 0 ; ?></td>
                        <td bgcolor="<?php echo ($sedes['diponibles_lectoras'] > 1 ? $verde : $rojo); ?>"><?php  echo isset($sedes['diponibles_lectoras']) ? $sedes['diponibles_lectoras'] : 0 ; ?></td>
                    </tr> 
                <?php
                    $cont++;
                    $sumNroOI = $sumNroOI + $sedes['sede_oi'];
                    $sumTablets = $sumTablets + $sedes['tablets'];
                    $sumLectoras = $sumLectoras + $sedes['lectoras'];
                    $sumTabletLectoras = $sumTabletLectoras + $sedes['tablet_lectora'];
                    $sumTabletInoperativa = $sumTabletInoperativa + $sedes['tablets_inoperativos'];
                    $sumLectoraInoperativa = $sumLectoraInoperativa + $sedes['letoras_inoperativos'];
                    $sumTabletLectoraOperativo = $sumTabletLectoraOperativo + $sedes['tablet_lectora_operativo'];
                    $sumExcedenteTablets = $sumExcedenteTablets + $sedes['excedente_tablets'];
                    $sumExcedenteLectoras = $sumExcedenteLectoras + $sedes['excedente_lectoras'];
                    $sumDisponibleTabletLectora = $sumDisponibleTabletLectora + $sedes['diponibles_tablet_lectora'];
                    $sumDisponibleTablet = $sumDisponibleTablet + $sedes['diponibles_tablets'];
                    $sumDisponibleLectora = $sumDisponibleLectora + $sedes['diponibles_lectoras'];
                } 
                ?>
                    <tr>
                        <td></td>
                        <td><b>Total:</b></td>
                        <td><b><?php  echo $sumNroOI ; ?></b></td>
                        <td><b><?php  echo $sumTablets ; ?></b></td>
                        <td><b><?php  echo $sumLectoras ; ?></b></td>
                        <td><b><?php  echo $sumTabletLectoras ; ?></b></td>
                        <td><b><?php  echo $sumTabletInoperativa ; ?></b></td>
                        <td><b><?php  echo $sumLectoraInoperativa ; ?></b></td>
                        <td><b><?php  echo $sumTabletLectoraOperativo ; ?></b></td>
                        <td><b><?php  echo $sumExcedenteTablets ; ?></b></td>
                        <td><b><?php  echo $sumExcedenteLectoras ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleTabletLectora ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleTablet ; ?></b></td>
                        <td><b><?php  echo $sumDisponibleLectora ; ?></b></td>
                    </tr>                    
            </tbody>            
          </table>

      </div>
    </div>
    
</div>

