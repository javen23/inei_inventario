<?php
//Esta variable continen los datos guardados en memoria
//Ejemplo: array(1) { 
//["sede"]=> array(3) { 
//      ["sede_id"]=> string(1) "1" 
//      ["sede_nombre"]=> string(21) "AMAZONAS-BAGUA GRANDE" 
//     ["nro_operador_informatico"]=> NULL } }

$session = $this->session->userdata("registra_equipos");
?>
<div class="container">

	<div class="row">
		<!--div class="col-md-3" style="text-align:center; padding:15px;"-->
                <div class="col-md-6" style="padding:15px;">    
			<strong>Seleccionar sede</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secadmin/registro_equipos_sede"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-sede" />
				<select class="form-control" name="sede_id">
					<option value="-1">Seleccione sede</option>
					<?php
					for($i = 0; $i < count($data["sedes"]); $i++){
						echo "<option value=\"".$data["sedes"][$i]["sede_id"]."\"".(isset($session["sede"]["sede_id"]) && $session["sede"]["sede_id"] == $data["sedes"][$i]["sede_id"] ? " selected" : "").">".htmlentities($data["sedes"][$i]["sede_nombre"])."</option>";
					}
					?>
				</select>
                                
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
		</div>
            
		<div class="col-md-3" style="text-align:center; padding:15px;">
			<?php                        
			if(isset($session["sede"]["sede_id"])) {
			?>
			<strong>Nro. Operador(es) Informático(s)</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secadmin/registro_equipos_sede"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="agregar-oi" />
                                <input type="hidden" name="sede_id" value="<?php echo $session["sede"]["sede_id"] ?>" /> 
                                <input class="form-control" id="oper_informatico" type="text" name="nro_oper_informatico" value="<?php echo $session["sede"]["nro_operador_informatico"]; ?>" style="width:25%;" />
				<input class="btn btn-default" type="submit" value="Modificar" />
			</form>                      
			<?php
			}
			?>
		</div>

		<div class="col-md-3" style="text-align:center; padding:15px;">
			<?php
			if(isset($session["sede"]["sede_id"])){
			?>
                        <strong>Reporte</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secadmin/equipos_oi_sedes_reporte"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="mostrar-equipos" />
				<input class="btn btn-primary" type="submit" value="EQUIPOS OI X SEDES" />
			</form>
			<?php
			}
			?>
		</div>
	</div>
	
	<?php
//	if(isset($message2)){
//		if($message2["type"] == "danger" || $message2["type"] == "warning" || $message2["type"] == "info" || $message2["type"] == "success"){
//			echo "<div class=\"container\"><div class=\"alert alert-".$message2["type"]."\" role=\"alert\">".htmlentities($message2["text"])."</div></div>";
//		}
//	}
	?>
	
	<br />
        
    <?php
    if(isset($data["crud_output"])){
    ?>
            <div>
                    <?php echo $data["crud_output"]; ?>
            </div>
    <?php
    }
    ?>

</div>
