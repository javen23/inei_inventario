<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1"><?php
if(isset($description)){
	echo "\n<meta name=\"description\" content=\"".htmlentities($description)."\" />";
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/bootstrap/css/bootstrap-theme.min.css"); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/theme/css/style.css"); ?>" /><?php
if(isset($css)){
	for($i = 0; $i < count($css); $i++){
		echo "\n<link rel=\"stylesheet\" type=\"text/css\" href=\"".$css[$i]."\" />";
	}
}
?>

<title><?php echo isset($title) ? htmlentities($title)." | " : ""; ?>Inventario Inei 1.0</title>

<script language="javascript" type="application/javascript" src="<?php echo base_url("assets/jquery/jquery-2.2.1.min.js"); ?>"></script>
<script language="javascript" type="application/javascript" src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script><?php
if(isset($js)){
	for($i = 0; $i < count($js); $i++){
		echo "\n<script language=\"javascript\" type=\"application/javascript\" src=\"".$js[$i]."\"></script>";
	}
}
?>

</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top"><!-- navbar-fixed-top / navbar-static-top-->
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url(""); ?>">Inventario Inei 1.0</a>
			<!--<img alt="AdminChow" src="img/logo.png" style="width:20px; height:20px;">-->
		</div>
		
		<div id="navbar" class="navbar-collapse collapse">
			<?php
			if($this->session->userdata("login_data")){
			?>
			<ul class="nav navbar-nav">
				<?php
				$login = $this->session->userdata("login_data");
				switch($login["perfil_id"]){
					case 1://super
					case 2://admin
				?>
				<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acciones<span class="caret"></span></a>
				<ul class="dropdown-menu">
				<li><a href="<?php echo base_url("secadmin/usuario"); ?>">Registrar Usuarios</a></li>
				<li><a href="<?php echo base_url("secadmin/registro_equipos_sede"); ?>">Registrar Equipos OI por Sedes</a></li>
				</ul>
				</li>
				<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("secudra/sede_recepcion_reporte"); ?>">Ítems recibidos en las sedes</a></li>
					<li><a href="<?php echo base_url("secadmin/equipos_oi_sedes_reporte"); ?>">Equipos OI por Sedes</a></li>
				</ul>
				</li>
				<!-- JHV:Fin -->
				

				<?php
						break;
					case 3://udra admin
					case 4://udra user
				?>
					<?php if($login["perfil_id"] == 3) : ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acciones<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url("secudra/inventario"); ?>">Registrar ítems en inventario</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?php echo base_url("secudra/asignacion"); ?>">Registrar asociación sede-item</a></li>
						<li><a href="#">Registrar asosiación sede-personal</a></li>
					</ul>
				</li>
					<?php endif; ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url("secudra/inventario_reporte"); ?>">ítems en inventario</a></li>
						<li role="separator" class="divider"></li>
						<!--<li><a href="<?php echo base_url("secudra/asignacion_reporte"); ?>">Asociación sede-ítem</a></li>
						<li><a href="#">Asociación sede-personal</a></li>
						<li><a href="#">Asociación personal-ítem</a></li>-->
						<li><a href="<?php echo base_url("secudra/sede_recepcion_reporte"); ?>">Ítems recibidos en las sedes</a></li>
						<li role="separator" class="divider"></li>
						<!--<li><a href="#">Ítems  devueltos</a></li>-->
						
					</ul>
				</li>
				<?php
						break;
					case 5://sede admin
					case 6://sede user
				?>
					<?php if($login["perfil_id"] == 5) : ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acciones<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url("secsede/recepcion"); ?>">Registrar ítems recibidos en la sede</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?php echo base_url("secsede/entregado_personal"); ?>">Registrar ítems entregados al personal</a></li>
						<!--<li><a href="#">Registrar ítems devueltos por el personal</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?php echo base_url("secsede/devuelto_almacen"); ?>">Registrar ítems devueltos a almacén</a></li>-->
					</ul>
				</li>
					<?php endif; ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url("secsede/recepcion_reporte"); ?>">Ítems recibidos en la sede</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?php echo base_url("secsede/entregado_personal_reporte"); ?>">Ítems entregados al personal</a></li>
						<!--<li><a href="<?php echo base_url("secsede/devuelto_personal_reporte"); ?>">Ítems devueltos por el personal</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?php echo base_url("secsede/devuelto_almacen_reporte"); ?>">Ítems devueltos a almacén</a></li>-->
					</ul>
				</li>
				<?php
						break;
				}
				?>
				
			<!--
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-home"></i> Usuarios<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Nuevo registro</a></li>
						<li><a href="#">Listar registros</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Perfiles</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Permisos</a></li>
						<li><a href="#">Reporte 1</a></li>
						<li><a href="#">Reporte 2</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Presupuesto<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Nuevo registro</a></li>
						<li><a href="#">Listar registros</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Reporte 1</a></li>
						<li><a href="#">Reporte 2</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header"><strong>Reportes de usuarios</strong></li>
						<li><a href="#">Reporte 1</a></li>
						<li><a href="#">Reporte 2</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header"><strong>Reportes de presupuesto</strong></li>
						<li><a href="#">Reporte 3</a></li>
						<li><a href="#">Reporte 4</a></li>
					</ul>
				</li>
			-->
				<!--<<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ayuda<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url("documentacion"); ?>">Documentación de la aplicación</a></li>
						<li><a href="<?php echo base_url("acerca"); ?>">Acerca de Inventario Inei 1.0</a></li>
					</ul>
				</li>-->
			</ul>
			<?php
			}else{
				// falta
			}
			?>
			<ul class="nav navbar-nav navbar-right">
				<!--<li class="active"><a href="./">Static top <span class="sr-only">(current)</span></a></li>-->
				<?php
				if($this->session->userdata("login_data")){
					$session = $this->session->userdata("login_data");
					echo "<li><a href=\"\">".htmlentities($session["usuario_nombre"])." (".htmlentities($session["perfil_nombre"]).")</a></li>";
				}
				
				?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi Cuenta<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php
						if($this->session->userdata("login_data")){
						?>
						<!--<li><a href="<?php echo base_url("perfil"); ?>">Perfil</a></li>
						<li><a href="<?php echo base_url("password"); ?>">Cambiar de contraseña</a></li>-->
						<li><a href="<?php echo base_url("logout"); ?>">Salir</a></li>
						<?php
						}else{
						?>
						<li><a href="<?php echo base_url("login"); ?>">Ingresar</a></li>
						<!--<li><a href="<?php echo base_url("password"); ?>">Cambiar de contraseña</a></li>-->
						<?php
						}
						?>
						
						
						
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>

<?php
if(isset($message)){
	if($message["type"] == "danger" || $message["type"] == "warning" || $message["type"] == "info" || $message["type"] == "success"){
		echo "<div class=\"container\"><div class=\"alert alert-".$message["type"]."\" role=\"alert\">".htmlentities($message["text"])."</div></div>";
	}
}
?>

<?php
if(isset($view)){
	/*if(isset($data)){
		$this->load->view($vista, $vista_data);
	}else{
		$this->load->view($vista);
	}*/
	$this->load->view($view);
}
?>

<footer class="footer">
	<div class="container">
		<!--<p class="pull-right"><a href="#">Back to top</a></p>-->
		<p class="text-muted text-center">&copy; 2016 INEI</p>
	</div>
</footer>

</body>
</html>