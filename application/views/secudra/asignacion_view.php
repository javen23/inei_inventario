<?php

$session = $this->session->userdata("asignacion_data");
//print_r($session["inventario"]);

?>
<div class="container">
	<!--<div class="page-header">
		<h1>Registrar inventario inicial</h1>
	</div>-->
<?php
	if(isset($title)){
		echo "<h2>".htmlentities($title)."</h2>";
	}
?>
	
	<div class="row">
		<div class="col-md-3" style="text-align:center; padding:15px;">
			<strong>Seleccionar sede</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secudra/asignacion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-sede" />
				<select class="form-control" name="sede_id" style="width:100%;">
					<option value="-1">Seleccione sede</option>
					<?php
					for($i = 0; $i < count($data["sedes"]); $i++){
						echo "<option value=\"".$data["sedes"][$i]["sede_id"]."\"".(isset($session["sede"]["sede_id"]) && $session["sede"]["sede_id"] == $data["sedes"][$i]["sede_id"] ? " selected" : "").">".htmlentities($data["sedes"][$i]["sede_nombre"])."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
		</div>
		<div class="col-md-3" style="text-align:center; padding:15px;">
			<?php
			if(isset($session["sede"]["sede_id"])){
			?>
			<strong>Seleccionar cargo</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secudra/asignacion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-cargo" />
				<select class="form-control" name="cargo_id" style="width:100%;">
					<option value="-1">Seleccione cargo</option>
					<?php
					for($i = 0; $i < count($data["cargos"]); $i++){
						echo "<option value=\"".$data["cargos"][$i]["cargo_id"]."\"".(isset($session["cargo"]["cargo_id"]) && $session["cargo"]["cargo_id"] == $data["cargos"][$i]["cargo_id"] ? " selected" : "").">".htmlentities($data["cargos"][$i]["cargo_nombre"])."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
			<?php
			}
			?>
		</div>
		<div class="col-md-3" style="text-align:center; padding:15px;">
			<?php
			if(isset($session["sede"]["sede_id"]) && isset($session["cargo"]["cargo_id"])){
			?>
			<strong>Ingresar código patrimonial</strong>
			<form class="form-inline" action="<?php echo base_url("secudra/asignacion"); ?>" method="post" enctype="application/x-www-form-urlencoded" onsubmit="return validar_form_3();">
				<input type="hidden" name="accion" value="agregar-codigo">
				<input class="form-control" id="codigo" type="text" name="codigo" placeholder="Ej. 00001" style="width:100%;" />
				<input class="btn btn-default" type="submit" value="Agregar" />
			</form>
			<script>
			function validar_form_3(){
				if($("#codigo").val().length != 5){
					alert("Ingrese 5 dígitos");
					$("#codigo").focus();
					return false;
				}
			}
			
			
			$("#codigo").keydown(function(event){
				
				var codes = [];
				//f1-f12 : 112 - 123
				if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)){
					if(event.shiftKey){
						event.preventDefault();
					}else{
						if(!($(this).val().length < 5)){
							event.preventDefault();
						}
					}
				}else if(event.keyCode == 90 || event.keyCode == 88 || event.keyCode == 67 || event.keyCode == 86){//z, x, c, v
					if(!event.ctrlKey){
						event.preventDefault();
					}
				}else if(event.keyCode == 8//backspace
					 || event.keyCode == 13//enter
					 || event.keyCode == 27//ESC
					 || event.keyCode == 35//FIN
					 || event.keyCode == 36//INICIO
					 || event.keyCode == 37//izquierda
					 || event.keyCode == 38//arriba
					 || event.keyCode == 39//derecha
					 || event.keyCode == 40//abajo
					 || event.keyCode == 46//DEL
					 ){
					// PERMITIR
				}else{
					console.log(event.keyCode);
					event.preventDefault();
				}
			});
			$("#codigo").focus();
			</script>
			<?php
			}
			?>
		</div>
		<div class="col-md-3" style="text-align:center; padding:15px;">
			<?php
			if(isset($session["sede"]["sede_id"]) && isset($session["cargo"]["cargo_id"]) && isset($session["inventario"]) && count($session["inventario"]) > 0){
			?>
			<strong>Asignar y terminar</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secudra/asignacion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="asignar-y-terminar" />
				<input class="btn btn-primary" type="submit" value="Asignar y terminar" style="width:100%;" />
			</form>
			<?php
			}
			?>
		</div>
	</div>
	
	<?php
	if(isset($message2)){
		if($message2["type"] == "danger" || $message2["type"] == "warning" || $message2["type"] == "info" || $message2["type"] == "success"){
			echo "<div class=\"container\"><div class=\"alert alert-".$message2["type"]."\" role=\"alert\">".htmlentities($message2["text"])."</div></div>";
		}
	}
	?>
	
	<br />
	
	<?php
	if(isset($session["sede"]["sede_id"]) && isset($session["cargo"]["cargo_id"]) && isset($session["inventario"]) && count($session["inventario"]) > 0){
	?>
	<div class="panel panel-default">
		<div class="panel-heading"><b>Reporte previo de asignación de items a la sedes</b> / <a href="<?php echo base_url("secudra/asignacion_reporte_xls"); ?>">Descargar reporte</a></div>
		<!--<div class="panel-body">
			<a href="" class="btn btn-primary">Agregar nuevo</a>
		</div>-->
		<table class="table">
		<tr>
			<th>Código</th>
			<th>Descripción</th>
			<th>Sede</th>
			<th>Cargo</th>
			<th>Eliminar</th>
		</tr>
	<?php
		foreach($session["inventario"] as $key => $value){
			echo "
		<tr>
			<td>".htmlentities($value["inventario_codigo"])."</td>
			<td>".htmlentities($value["inventario_descripcion"])."</td>
			<td>".htmlentities($value["sede_nombre"])."</td>
			<td>".htmlentities($value["cargo_nombre"])."</td>
			<td>
				<form action=\"".base_url("secudra/asignacion")."\" method=\"post\" enctype=\"application/x-www-form-urlencoded\" onsubmit=\"return confirm('Está seguro de eliminar el código ".htmlentities($value["inventario_codigo"])."')\">
				<input type=\"hidden\" name=\"accion\" value=\"eliminar-codigo\" />
				<input type=\"hidden\" name=\"codigo\" value=\"".$value["inventario_codigo"]."\" />
				<input type=\"submit\" value=\"X\" />
				</form>
			</td>
		</tr>";
		}
	?>
		</table>
		<div class="panel-footer"><b>Reporte previo de asignación de items a la sedes</b></div>
	</div>
	<?php
	}
	?>
</div>
