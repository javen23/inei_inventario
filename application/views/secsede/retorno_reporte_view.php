<?php
$session = $this->session->userdata("retorno_reporte_data");
?>
<div class="container">
	<h2><?php echo htmlentities($title); ?></h2>
	
	<div class="row">
		<div class="col-md-12" style="padding:15px;">
<?php
if(count($data["sedes"]) > 0){
?>
			<strong>Seleccionar sede</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secsede/retorno_reporte"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-sede" />
				<select class="form-control" name="sede_id">
					<option value="-1">Seleccione sede</option>
					<?php
					for($i = 0; $i < count($data["sedes"]); $i++){
						echo "<option value=\"".$data["sedes"][$i]["sede_id"]."\"".(isset($session["sede"]["sede_id"]) && $session["sede"]["sede_id"] == $data["sedes"][$i]["sede_id"] ? " selected" : "").">".htmlentities($data["sedes"][$i]["sede_nombre"])."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
<?php
}
?>
		</div>
	</div>
	
<?php
if(isset($message2)){
	if($message2["type"] == "danger" || $message2["type"] == "warning" || $message2["type"] == "info" || $message2["type"] == "success"){
		echo "<div class=\"container\"><div class=\"alert alert-".$message2["type"]."\" role=\"alert\">".htmlentities($message2["text"])."</div></div>";
	}
}
?>

	<!--<style type='text/css'>
	/*body{
	font-family: Arial;
	font-size: 14px;
	}
	a {
	color: blue;
	text-decoration: none;
	font-size: 14px;
	}
	a:hover
	{
	text-decoration: underline;
	}
	</style>-->
<?php
if(isset($data["crud_output"])){
?>
	<div>
		<?php echo $data["crud_output"]; ?>
	</div>
<?php
}
?>
</div>