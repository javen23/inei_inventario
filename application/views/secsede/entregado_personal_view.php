<?php
$session = $this->session->userdata("entregado_personal_data");

?>

<div class="container">
	
<?php
	if(isset($title)){
		echo "<h2>".$title."</h2>";
	}
?>

	<div class="row">
		<div class="col-md-4" style="text-align:center; padding:15px;">
<?php
if(count($data["sedes"]) > 0){
?>
			<strong>Seleccionar sede</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secsede/entregado_personal"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-sede" />
				<select class="form-control" name="sede_id">
					<option value="-1">Seleccione sede</option>
					<?php
					for($i = 0; $i < count($data["sedes"]); $i++){
						echo "<option value=\"".$data["sedes"][$i]["sede_id"]."\"".(isset($session["sede"]["sede_id"]) && $session["sede"]["sede_id"] == $data["sedes"][$i]["sede_id"] ? " selected" : "").">".$data["sedes"][$i]["sede_nombre"]."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
<?php
}
?>
		</div>
		<div class="col-md-4" style="text-align:center; padding:15px;">
<?php
if(isset($session["sede"]["sede_id"])){
?>
			<strong>Seleccionar personal</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secsede/entregado_personal"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-personal" />
				<select class="form-control" name="personal_id" style="width:100%;">
					<option value="-1">Seleccione personal</option>
					<?php
					for($i = 0; $i < count($data["personal"]); $i++){
						echo "<option value=\"".$data["personal"][$i]["personal_id"]."\"".(isset($session["personal"]["personal_id"]) && $session["personal"]["personal_id"] == $data["personal"][$i]["personal_id"] ? " selected" : "").">".$data["personal"][$i]["personal_dni"]." - ".$data["personal"][$i]["personal_nombre"]."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
<?php
}
?>
		</div>
		<div class="col-md-4" style="text-align:center; padding:15px;">
<?php
if(isset($session["sede"]["sede_id"]) && isset($session["personal"]["personal_id"])){
?>
			<strong>Ingresar código patrimonial</strong>
			<form class="form-inline" action="<?php echo base_url("secsede/entregado_personal"); ?>" method="post" enctype="application/x-www-form-urlencoded" onsubmit="return validar_form_3();">
				<input type="hidden" name="accion" value="agregar-item">
				<input class="form-control" id="codigo" type="text" name="codigo" placeholder="Ej. 00001" style="width:100%;" />
				<input class="btn btn-default" type="submit" value="Agregar" />
			</form>
<?php
}
?>
		</div>
		<div class="col-md-12" style="text-align:center; padding:15px;">
<?php
if(isset($session["items"]) && count($session["items"]) > 0){
?>
			<strong>Terminar y guardar</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secsede/entregado_personal"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="terminar-y-guardar" />
				<input class="btn btn-primary" type="submit" value="Terminar y guardar" />
			</form>
<?php
}
?>
		</div>
	</div>


<?php
if(isset($message2)){
	if($message2["type"] == "danger" || $message2["type"] == "warning" || $message2["type"] == "info" || $message2["type"] == "success"){
		echo "<div class=\"container\"><div class=\"alert alert-".$message2["type"]."\" role=\"alert\">".$message2["text"]."</div></div>";
	}
}
?>


<?php
if(isset($session["items"]) && count($session["items"]) > 0){
?>
	<div class="panel panel-default">
		<div class="panel-heading"><b>Reporte previo al registro de ítems entregados al personal</b> / <a href="<?php echo base_url("secsede/entregado_personal_reporte_xls"); ?>">Descargar reporte</a></div>
		<!--<div class="panel-body">
			<a href="" class="btn btn-primary">Agregar nuevo</a>
		</div>-->
		<table class="table">
		<tr>
			<th>Sede</th>
			<th>DNI</th>
			<th>Personal</th>
			<th>Item</th>
			<th>Descripción</th>
			<th>Eliminar</th>
		</tr>
	<?php
		foreach($session["items"] as $key => $value){
			echo "
		<tr>
			<td>".$value["sede_nombre"]."</td>
			<td>".$value["personal_dni"]."</td>
			<td>".$value["personal_nombre"]."</td>
			<td>".$value["inventario_codigo"]."</td>
			<td>".$value["inventario_descripcion"]."</td>
			<td>
				<form action=\"".base_url("secsede/entregado_personal")."\" method=\"post\" enctype=\"application/x-www-form-urlencoded\" onsubmit=\"return confirm('Está seguro de eliminar el código ".$value["inventario_codigo"]."')\">
				<input type=\"hidden\" name=\"accion\" value=\"eliminar-item\" />
				<input type=\"hidden\" name=\"codigo\" value=\"".$value["inventario_codigo"]."\" />
				<input type=\"submit\" value=\"X\" />
				</form>
			</td>
		</tr>";
		}
	?>
		</table>
		<div class="panel-footer"><b>Reporte previo al registro de ítems entregados al personal</b></div>
	</div>
<?php
}
?>
	
</div>
