<?php
$session = $this->session->userdata("user_data");
?>

<div class="container">
	
<?php
	if(isset($title)){
		echo "<h2>".htmlentities($title)."</h2>";
	}
?>

	<div class="row">
		<div class="col-md-6" style="text-align:center; padding:15px;">
<?php
if(count($data["sedes"]) > 0){
?>
			<strong>Seleccionar sede</strong>
			<form class="form-inline" role="form" action="<?php echo base_url("secsede/devolucion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="seleccionar-sede" />
				<select class="form-control" name="sede_id">
					<option value="-1">Seleccione sede</option>
					<?php
					for($i = 0; $i < count($data["sedes"]); $i++){
						echo "<option value=\"".$data["sedes"][$i]["sede_id"]."\"".(isset($session["sede"]["sede_id"]) && $session["sede"]["sede_id"] == $data["sedes"][$i]["sede_id"] ? " selected" : "").">".htmlentities($data["sedes"][$i]["sede_nombre"])."</option>";
					}
					?>
				</select>
				<input class="btn btn-default" type="submit" value="Seleccionar" />
			</form>
<?php
}
?>
		</div>
		<div class="col-md-6" style="text-align:center; padding:15px;">
<?php
if(isset($session["sede"]["sede_id"])){
?>
			<strong>Consultar código patrimonial</strong>
			<form id="form1" class="form-inline" role="form" action="<?php echo base_url("secsede/devolucion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name="accion" value="consultar-codigo">
				<input class="form-control" id="codigo" type="text" name="codigo" placeholder="Ej. 00001" />
				<input class="btn btn-default" type="submit" value="Consultar" />
			</form>
			<script>
			/*if($("#observacion").length){
				$("#observacion").focus();
			}else{
				
			}*/
			
			
			$("#codigo").keydown(function(event){
				
				/*var code = {
					ranges : [{1, 5}, 3, 7],
				};*/
				//f1-f12 : 112 - 123
				if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode >= 112 && event.keyCode <= 123)){					if(event.shiftKey){
						event.preventDefault();
					}else{
						if(!($(this).val().length < 5)){
							event.preventDefault();
						}
					}
				}else if(event.keyCode == 90 || event.keyCode == 88 || event.keyCode == 67 || event.keyCode == 86){//CTRL + z, x, c, v
					if(!event.ctrlKey){
						event.preventDefault();
					}
				}else if(event.keyCode == 8//backspace
					 || event.keyCode == 9//TAB
					 || event.keyCode == 13//enter
					 || event.keyCode == 27//ESC
					 || event.keyCode == 35//FIN
					 || event.keyCode == 36//INICIO
					 || event.keyCode == 37//izquierda
					 || event.keyCode == 38//arriba
					 || event.keyCode == 39//derecha
					 || event.keyCode == 40//abajo
					 || event.keyCode == 46//DEL
					 ){
					// PERMITIR
				}else{
					console.log(event.keyCode);
					event.preventDefault();
				}
			});
			$("#codigo").focus();
			</script>
<?php
}
?>
		</div>
	</div>


<?php
if(isset($message2)){
	if($message2["type"] == "danger" || $message2["type"] == "warning" || $message2["type"] == "info" || $message2["type"] == "success"){
		echo "<div class=\"container\"><div class=\"alert alert-".$message2["type"]."\" role=\"alert\">".htmlentities($message2["text"])."</div></div>";
	}
}
?>


<?php
if(isset($session["inventario"]["inventario_codigo"])){
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Información del equipo</b>
		</div>
		
		<div class="row" style="margin:5px;">
			<div class="col-md-3">
				<b>Código</b><br />
				<?php echo htmlentities($session["inventario"]["inventario_codigo"]); ?>
			</div>
			<div class="col-md-3">
				<b>Descripción</b><br />
				<?php echo htmlentities($session["inventario"]["inventario_descripcion"]); ?>
			</div>
			<div class="col-md-3">
				<b>Sede asignada</b><br />
				<?php echo htmlentities($session["inventario"]["sede_nombre"]); ?>
			</div>
			<div class="col-md-3">
				<b>Observaciónes de salida</b><br />
				<?php echo htmlentities($session["inventario"]["m0_registrado_observacion"]); ?>
			</div>
		</div>
		<form action="<?php echo  base_url("secsede/devolucion"); ?>" method="post" enctype="application/x-www-form-urlencoded">
		<input type="hidden" name="accion" value="registrar-devolucion" />
		<input type="hidden" name="codigo" value="<?php echo $session["inventario"]["inventario_codigo"]; ?>" />
		<input type="hidden" name="sede_id" value="<?php echo $session["inventario"]["sede_id"]; ?>" />
		<div class="row" style="margin:5px;">
			<div class="col-md-12">
				<div style="float:left;"><b>Observaciones al momento de la devolución</b></div>
				<textarea id="observacion" name="observacion" class="form-control" style="width:100%; height:80px; min-width:0px;" maxlength="200" placeholder="Escriba aquí las observaciones de devolución."></textarea>
				<div style="float:right;">Máximo número de caracteres <span id="contador">0/200</span></div>
				<script>
				$("#observacion").keyup(function(event){
					$("#contador").text($("#observacion").val().length + "/200");
				});
				</script>
			</div>
			<div class="col-md-12" style="text-align:center;">
				<input class="btn btn-primary" type="submit" value="Registrar devolución" />
			</div>
		</div>
		
		
		</form>
		
	</div>
<?php
}
?>







	
	
	
	<script language="javascript" type="application/javascript">
	// aqui las restricciones de la interfaz de usuario
	
	
	// aqui la validacion del form
	
	
	</script>
	
	
</div>
