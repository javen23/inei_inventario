--
-- Create schema inei_inventario
--

CREATE DATABASE IF NOT EXISTS inei_inventario;
USE inei_inventario;

--
-- Definition of table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `cargo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cargo_nombre` varchar(45) DEFAULT NULL,
  `cargo_creado_por` int(10) unsigned DEFAULT NULL,
  `cargo_creado_fecha` datetime DEFAULT NULL,
  `cargo_modificado_por` int(10) unsigned DEFAULT NULL,
  `cargo_modificado_fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`cargo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargo`
--

/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` (`cargo_id`,`cargo_nombre`,`cargo_creado_por`,`cargo_creado_fecha`,`cargo_modificado_por`,`cargo_modificado_fecha`) VALUES
 (1,'CARGO NO DEFINIDO',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (2,'APLICADOR',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (3,'ASISTENTE ADMINISTRATIVO',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (4,'ASISTENTE DE COORDINADOR DE LOCAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (5,'ASISTENTE INFORMÁTICO DE LOCAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (6,'COORDINADOR DE LOCAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (7,'COORDINADOR DE SEDE',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (8,'COORDINADOR DE SEGURIDAD DE SEDE',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (9,'COORDINADOR LÍDER DE LOCAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (10,'DIRECTOR DE LA ODEI',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (11,'EQUIPOS DE CONTINGENCIA',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (12,'INFORMÁTICO DE LOCAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (13,'MONITOR INEI',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (14,'OPERADOR INFORMÁTICO',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (15,'ORIENTADOR',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (16,'SUPERVISOR INFORMÁTICO',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01'),
 (17,'SUPERVISOR NACIONAL',1,'2016-03-17 11:01:01',1,'2016-03-17 11:01:01');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;


--
-- Definition of table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
CREATE TABLE `inventario` (
  `inventario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventario_codigo` varchar(5) NOT NULL,
  `inventario_descripcion` varchar(45) DEFAULT NULL,
  `inventario_observacion` text,
  `inventario_fecha` datetime DEFAULT NULL,
  `asignado_sede_id` int(10) unsigned DEFAULT NULL,
  `asignado_cargo_id` int(10) unsigned DEFAULT NULL,
  `asignado_fecha` datetime DEFAULT NULL,
  `recibido_por_usuario_id` int(10) unsigned DEFAULT NULL,
  `recibido_observacion` text,
  `recibido_fecha` datetime DEFAULT NULL,
  `distribuido_por_usuario_id` int(10) unsigned DEFAULT NULL,
  `distribuido_a_usuario_id` int(10) unsigned DEFAULT NULL,
  `distribuido_fecha` datetime DEFAULT NULL,
  `devuelto_observacion` text,
  `devuelto_fecha` datetime DEFAULT NULL,
  `retorno_por_usuario_id` int(10) unsigned DEFAULT NULL,
  `retorno_observacion` text,
  `retorno_defecto` char(1) DEFAULT NULL,
  `retorno_fecha` datetime DEFAULT NULL,
  `verificado_por_usuario_id` int(10) unsigned DEFAULT NULL,
  `verificado_observacion` text,
  `verificado_fecha` datetime DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`inventario_id`),
  UNIQUE KEY `inventario_codigo` (`inventario_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventario`
--

INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65588','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60352','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65587','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65539','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65556','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65573','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61308','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60699','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60905','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61085','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55432','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56274','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60158','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53566','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55668','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53806','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60949','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61417','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60704','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60738','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53568','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65582','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65589','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60315','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60387','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60298','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60388','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53565','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65608','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65605','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60868','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61286','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60987','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61393','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61301','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60456','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61092','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60447','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55603','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60421','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('66624','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53567','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60259','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60310','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53570','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56554','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53934','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54550','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55689','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55137','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61131','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61100','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61392','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60443','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61253','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61082','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60772','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61228','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61397','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61230','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65597','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59602','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59599','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56572','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54977','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54959','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56601','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56586','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59640','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56607','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54966','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56624','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54974','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62186','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55069','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61427','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61146','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61090','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61237','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60789','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60989','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61166','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61149','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61415','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60547','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61310','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63640','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63653','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63560','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65400','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63519','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63648','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63698','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63637','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63725','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63615','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63645','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63601','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63736','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63701','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63551','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65402','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63680','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63558','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63626','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63669','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63720','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63606','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65389','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61651','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61888','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61556','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61808','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61704','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61468','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61529','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61816','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61514','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61563','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61777','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61559','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61521','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61643','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61499','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61855','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61536','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
/*INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61655','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');*/
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61678','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62187','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54875','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54976','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62190','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54878','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56592','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54933','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56576','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54882','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59604','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55036','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62144','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59610','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54923','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54944','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56613','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54820','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61547','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61571','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61662','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61858','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61831','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61811','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61511','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61655','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61772','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60926','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60798','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60609','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60927','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54492','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54247','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('66748','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54201','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63590','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63644','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63625','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63737','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63564','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63600','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60232','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60242','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55719','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60215','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55405','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59533','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55335','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55710','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59554','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59546','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55382','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55330','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62026','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62020','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54881','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54862','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59645','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56610','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59893','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61017','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61347','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61168','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61016','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61012','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61162','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61020','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61377','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61169','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61047','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61032','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61179','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61104','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61279','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61048','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61652','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61624','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61676','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61792','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61784','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61805','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61578','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61483','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62207','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62202','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59946','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55250','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59548','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53890','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54078','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54357','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56517','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54094','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60025','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53844','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55760','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55533','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54286','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65723','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55440','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54113','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60241','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62201','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60545','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61086','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60563','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60534','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60565','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60944','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60915','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60494','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60965','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61342','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61209','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60497','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61070','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60500','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60870','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61218','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55043','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59801','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56064','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54536','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54932','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53798','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59861','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53654','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59805','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61579','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61491','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61796','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61512','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61632','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61804','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61882','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55683','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54681','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53721','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53928','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59765','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55474','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54695','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54547','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55477','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54065','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59934','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59739','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59725','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56266','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54648','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61494','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61607','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61489','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61856','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61860','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61894','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61830','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61800','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61616','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61603','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61677','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54649','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56581','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54373','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55643','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62167','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54539','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
/*INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55673','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');*/
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54041','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55676','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('56396','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55647','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55754','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63635','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59937','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59527','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61877','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61300','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60756','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61382','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61174','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61363','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60757','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60498','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61177','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61410','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60621','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60062','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60079','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62203','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60104','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60063','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60056','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59997','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62204','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60053','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62142','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60057','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60051','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60035','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59967','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60097','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60050','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60047','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59828','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60030','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54788','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60014','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59958','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60039','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61650','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61785','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61803','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61480','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61582','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61522','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61466','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61566','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61701','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61706','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61691','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61502','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61875','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61707','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61705','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61702','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61572','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61915','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61539','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55316','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54835','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59999','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60068','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60094','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60003','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60091','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60034','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60000','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60016','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55008','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55007','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55066','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54896','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55585','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54970','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55009','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55337','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54972','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54869','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61067','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61150','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61088','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61112','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61348','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60458','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60752','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60755','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61126','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60824','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61050','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61142','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61244','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61040','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60450','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61216','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54874','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54842','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55321','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54824','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55328','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55484','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55099','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54857','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55612','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54872','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54839','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55037','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55611','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54971','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54879','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62145','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55614','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62149','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60783','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60606','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60629','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60672','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60745','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61289','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60485','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60583','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61318','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60935','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60597','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60477','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60708','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60475','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60608','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55650','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55634','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55673','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59534','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55775','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55748','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62158','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60580','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60632','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60586','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60603','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60680','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55684','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55795','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55776','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62155','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62165','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55645','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55470','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55711','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62164','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60707','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60646','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60861','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60917','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60723','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60888','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60693','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55712','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55808','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62154','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55644','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55753','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55708','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55638','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62153','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54981','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62161','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61389','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61265','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61386','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60821','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60825','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61163','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60902','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61272','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63651','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63523','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63659','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65399','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65401','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63684','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60710','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60725','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60705','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60546','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54125','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54053','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60268','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54229','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54142','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60616','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60924','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60582','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54211','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54360','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55012','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54833','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54848','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55202','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55520','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61294','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60919','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61385','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61171','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60478','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54342','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61935','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61931','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62229','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61944','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61939','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54292','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61080','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60482','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60551','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60951','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60541','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63692','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63695','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55103','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55248','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55170','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55183','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55523','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55508','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55100','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55107','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55206','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55220','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62215','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55519','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55516','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55342','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55433','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55438','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62053','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55586','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60140','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62219','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61381','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61160','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60747','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61734','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61520','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61876','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61610','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61590','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61798','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61870','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61516','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61910','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61617','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61583','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61826','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61482','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61585','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61485','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63617','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63696','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65395','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63711','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('53652','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63740','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59864','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63685','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59842','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63575','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63598','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65391','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63714','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63586','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63574','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59812','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60560','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60938','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60869','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60434','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60967','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60537','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60445','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60894','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60975','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60441','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60994','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60432','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63545','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63528','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63681','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63530','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63686','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('59816','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63579','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63687','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63563','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63629','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65396','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63679','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63690','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63667','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63583','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63623','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('63712','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60998','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60520','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60858','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61189','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60688','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60518','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60519','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60833','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60895','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60903','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60526','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60904','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60502','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62044','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62216','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55496','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55462','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62217','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62017','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62028','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60911','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60619','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61423','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60802','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60784','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55310','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55565','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65512','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('65470','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60267','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55502','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55524','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61934','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('55372','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60509','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61229','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61037','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61408','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61035','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60918','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60941','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61940','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54238','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61930','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54322','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60342','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54368','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60330','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60360','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60325','TABLET YOGA 8.',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60972','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60728','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60444','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61069','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61031','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61211','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61220','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60340','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62209','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60322','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60343','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60349','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61932','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61943','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61946','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60956','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60613','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60440','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61599','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60769','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60550','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61948','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61942','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('54341','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61933','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62208','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60346','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('60338','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('62228','TABLET YOGA 8.2',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61615','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61897','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61591','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61545','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61664','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');
INSERT INTO inventario (inventario_codigo,inventario_descripcion,inventario_observacion,inventario_fecha) VALUES ('61893','LECTORA DE CODIGO DE BARRA BARCODE CT10X',NULL,'2016-09-05 12:19:17');



--
-- Definition of table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `perfil_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `perfil_nombre` varchar(45) DEFAULT NULL,
  `perfil_estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perfil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfil`
--

/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`perfil_id`,`perfil_nombre`,`perfil_estado`) VALUES 
 (1,'SUPER',NULL),
 (2,'ADMIN',NULL),
 (3,'UDRA_ADMIN',NULL),
 (4,'UDRA_USER',NULL),
 (5,'SEDE_ADMIN',NULL),
 (6,'SEDE_USER',NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


--
-- Definition of table `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE `personal` (
  `personal_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `personal_dni` char(8) DEFAULT NULL,
  `personal_nombre` varchar(64) DEFAULT NULL,
  `sede_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`personal_id`),
  UNIQUE KEY `personal_dni` (`personal_dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal`
--

/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;


--
-- Definition of table `sede`
--

DROP TABLE IF EXISTS `sede`;
CREATE TABLE `sede` (
  `sede_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sede_nombre` varchar(45) DEFAULT NULL,
  `sede_creado_por` int(10) unsigned DEFAULT NULL,
  `sede_creado_fecha` datetime DEFAULT NULL,
  `sede_modificado_por` int(10) unsigned DEFAULT NULL,
  `sede_modificado_fecha` datetime DEFAULT NULL,
  `sede_oi` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`sede_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sede`
--

/*!40000 ALTER TABLE `sede` DISABLE KEYS */;
INSERT INTO `sede` (`sede_id`,`sede_nombre`,`sede_creado_por`,`sede_creado_fecha`,`sede_modificado_por`,`sede_modificado_fecha`,`sede_oi`) VALUES 
 (1,'AMAZONAS-BAGUA GRANDE',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',9),
 (2,'AMAZONAS-CHACHAPOYAS',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',7),
 (3,'ANCASH-CHIMBOTE',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',13),
 (4,'ANCASH-HUARAZ',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',37),
 (5,'APURIMAC-ABANCAY',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',8),
 (6,'APURIMAC-ANDAHUAYLAS',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',7),
 (7,'AREQUIPA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',35),
 (8,'AYACUCHO-HUAMANGA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',17),
 (9,'AYACUCHO-PUQUIO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',6),
 (10,'CAJAMARCA-CAJAMARCA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',33),
 (11,'CAJAMARCA-JAEN',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',8),
 (12,'CUSCO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',47),
 (13,'HUANCAVELICA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',10),
 (14,'HUANUCO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',26),
 (15,'ICA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',19),
 (16,'JUNIN',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',47),
 (17,'LA LIBERTAD',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',35),
 (18,'LAMBAYEQUE',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',24),
 (19,'LIMA METROPOLITANA-CALLAO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',152),
 (20,'LIMA PROVINCIA SUR CAÑETE',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',15),
 (21,'LIMA PROVINCIA NORTE HUACHO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',23),
 (22,'LORETO-IQUITOS',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',19),
 (23,'LORETO-YURIMAGUAS',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',4),
 (24,'MADRE DE DIOS',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',3),
 (25,'MOQUEGUA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',6),
 (26,'PASCO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',7),
 (27,'PIURA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',50),
 (28,'PUNO-JULIACA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',20),
 (29,'PUNO-PUNO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',13),
 (30,'SAN MARTIN-MOYOBAMBA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',7),
 (31,'SAN MARTIN-TARAPOTO',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',17),
 (32,'TACNA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',6),
 (33,'TUMBES',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',15),
 (34,'UCAYALI-PUCALLPA',1,'2016-03-17 15:25:29',1,'2016-03-17 15:25:29',18);
/*!40000 ALTER TABLE `sede` ENABLE KEYS */;


--
-- Definition of table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_username` varchar(45) DEFAULT NULL,
  `usuario_password` varchar(45) DEFAULT NULL,
  `perfil_id` int(10) unsigned DEFAULT NULL,
  `cargo_id` int(10) unsigned DEFAULT NULL,
  `usuario_dni` char(8) DEFAULT NULL,
  `usuario_nombre` varchar(45) DEFAULT NULL,
  `usuario_email` varchar(45) DEFAULT NULL,
  `usuario_telefono` varchar(45) DEFAULT NULL,
  `usuario_estado` varchar(45) DEFAULT NULL,
  `usuario_creado_por` int(10) unsigned DEFAULT 1,
  `usuario_creado_fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificado_por` int(10) unsigned DEFAULT NULL,
  `usuario_modificado_fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  UNIQUE KEY `usuario_username` (`usuario_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('super','INEI****',1,1,'00000000','Super Administrador');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('admin','INEI****',2,1,'00000000','Administrador');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('udra_user1','1234',3,1,'00000000','Usuario de udra 1');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('udra_user2','1234',4,1,'00000000','Usuario de udra 2');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('ORUIZ','INEI8508',6,16,'46724273','RUIZ SEGURA, OSBER NENE');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JMALDONADO','INEI4886',6,16,'46158816','MALDONADO MENDOZA, JOSE JEINER');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('ERODRIGUEZ','INEI8804',6,16,'70015994','RODRIGUEZ FLORES, EDUARDO RUBEN');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('MJAMANCA','INEI5430',6,16,'09745771','JAMANCA RAMIREZ, MARCO ANTONIO');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('EGOMEZ','INEI7288',6,16,'31044654','GOMEZ AIQUIPA, EBERT');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JMONZON','INEI3084',6,16,'47555557','MONZON FLORES, JAINE ALEXIA');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('WQUISPE','INEI2335',6,16,'42479147','QUISPE LOPEZ, WILSON CRISTHIAN');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('YCARRANZA','INEI8941',6,16,'41611381','CARRANZA LUJAN, YELINA LIZ');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('CCAMARGO','INEI3786',6,16,'43357142','CAMARGO BARRIENTOS, CESAR AUGUSTO');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JMONTENEGRO','INEI5598',6,16,'42299461','MONTENEGRO QUISPE, JOSE CARLOS');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JDIAZ','INEI8782',6,16,'27735580','DIAZ DAVILA, JOSE WILSON');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JSANCHEZ','INEI8563',6,16,'41820592','SANCHEZ QUISPE, JOSE LUIS');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('YSULLCA','INEI2476',6,16,'46412047','SULLCA FERNANDEZ, YHURI FERMIN');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('HTORRES','INEI9616',6,16,'41661073','TORRES CURILLA, HENRY MICHAEL');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('FELIAS','INEI7329',6,16,'72928087','ELIAS PALLIN, FABIOLA NATALY');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('IPEREZ','INEI0326',6,16,'42895130','PEREZ SOLIS, IVAN GORKI');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('EGUTIERREZ','INEI1383',6,16,'42191776','GUTIERREZ RUIZ, ERIK IVAN');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JMONTALVAN','INEI3994',6,16,'41939488','MONTALVAN ALFARO, JORGE WALTER');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('KESCALANTE','INEI9365',5,16,'44535652','ESCALANTE ZEGARRA, KARINA DENGSE');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JSILVERA','INEI0715',5,16,'44123913','SILVERA GALINDO, JOSE JOSIMAR');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('PGUTARRA','INEI2389',5,16,'09861606','GUTARRA MONTALVO, PERCY ROLY');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('PVERA','INEI9459',6,16,'41709618','VERA CUZCANO, PEDRO ERNESTO');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('LCHINGEL','INEI6684',6,16,'16725608','CHINGUEL CARRANZA, LANDER');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JPEREZ','INEI2576',6,16,'70778393','PEREZ, MRDINA, JEAN PIERRE');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JVARGAS','INEI3983',6,16,'43380455','VARGAS OCMIN, JACK PERCY');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('MMATEO','INEI8799',6,16,'42208494','MATEO PANDURO, MARUJA');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('RGARCIA','INEI2296',6,16,'40384639','GARCIA GUZMAN, RICARDO MARTIN');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('RVALLE','INEI9237',6,16,'42809699','VALLE MONTES, RAUL');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('DVASQUEZ','INEI7870',6,16,'43221674','VASQUEZ CALLE, DARWIN PABEL');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('WTICONA','INEI1067',6,16,'45763904','TICONA YANAPA, WALDIR RENATO');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JQUISPE','INEI7917',6,16,'44536805','QUISPE QUISPE, JUAN CARLOS');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JZURITA','INEI2701',6,16,'43631225','ZURITA MOZOMBITE, JULIO CESAR');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('GCASTILLO','INEI2389',6,16,'70233219','CASTILLO ARMAS, GIAN POLL');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('HCHAGUA','INEI6134',6,16,'42728748','CHAGUA YAURI, HENRY');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('JINGA','INEI6536',6,16,'01161389','INGA CHIROQUE, JAIME GERARDO');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('FDIAZ','INEI3104',6,16,'43397181','DIAZ VALLES, FRANK ZILER');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('daniel','OR5ITR',2,1,'00000000','Daniel');
INSERT INTO `usuario` (`usuario_username`,`usuario_password`,`perfil_id`,`cargo_id`,`usuario_dni`,`usuario_nombre`) VALUES ('consulta','G4JF85',4,1,'00000004','Solo consulta');


--
-- Definition of table `usuario_sede`
--

DROP TABLE IF EXISTS `usuario_sede`;
CREATE TABLE `usuario_sede` (
  `usuario_id` int(10) unsigned DEFAULT NULL,
  `sede_id` int(10) unsigned DEFAULT NULL,
  `creado_por` int(10) unsigned DEFAULT NULL,
  `creado_fecha` datetime DEFAULT NULL,
  `modificado_por` int(10) unsigned DEFAULT NULL,
  `modificado_fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario_sede`
--

/*!40000 ALTER TABLE `usuario_sede` DISABLE KEYS */;
INSERT INTO `usuario_sede` (`usuario_id`,`sede_id`,`creado_por`,`creado_fecha`,`modificado_por`,`modificado_fecha`) VALUES 
 (5,1,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (6,2,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (7,3,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (8,4,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (9,5,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (10,6,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (11,7,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (12,8,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (13,9,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (14,10,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (15,11,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (16,12,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (17,13,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (18,14,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (19,15,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (20,16,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (21,17,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (22,18,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (23,19,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (24,19,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (25,19,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (26,20,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (27,21,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (28,22,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (29,23,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (30,24,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (31,25,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (32,26,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (33,27,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (34,28,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (35,29,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (36,30,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (37,31,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (38,32,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (39,33,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08'),
 (40,34,2,'2016-03-29 12:58:08',2,'2016-03-29 12:58:08');
/*!40000 ALTER TABLE `usuario_sede` ENABLE KEYS */;

